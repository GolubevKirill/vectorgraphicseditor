#define BT_CLOSE 9
#define BT_ENTER 11
#define IDM_PALETTE 10

#define MI_OPEN 101
#define MI_SAVE 102
#define MI_SAVE_AS 103
#define MI_EXPORT_SVG 104
#define MI_EXIT 106

#define MI_LAYER_UP1 114
#define MI_LAYER_DOWN1 115
#define MI_LAYER_UP 116
#define MI_LAYER_DOWN 117
#define MI_LAYER_DEL 118

#define MI_ROLATION_R 120
#define MI_ROLATION_L 121

#define MI_FONT 124

#define MI_ABOUT 125
