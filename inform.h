#include <windows.h>
#include <vector>
#include <string>
//#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

struct Point       //��������� ��� �������� ����� ������������ �����
{
    Point(int x = 0, int y = 0) : x(x), y(y) {}
    int x;
    int y;
};

struct INFO
{
    int i;
    int X;
    int Y;
    int R1;
    int R2;
    int N;
    int W;
    int H;
    COLORREF c1;
    COLORREF c2;
    string st;
    RECT rect;
};

class object
{
public:
    int     X,Y;         //���������� ������
    bool top,left;
    int i;
    POINT   *p;
    POINT   *vertex;
    vector<Point> points;
    vector<Point>::iterator it;
    CHOOSEFONT cf;
    LOGFONT lf;
public:
    virtual void draw(HDC*) = 0;             //������� ���������
    virtual bool accessory(int,int) = 0;        //�������������� ������� ������� ������
    virtual int  accessory_bord(int,int) = 0;        //�������������� ������� ������� ������
    virtual void select_transform(int,int,int) = 0; //���������������� ������
    virtual void draw_transform(HDC*,bool) = 0;   //�������� ������ ��� �������������
    virtual void move_obj(int,int) = 0;
    virtual void fill_obj(COLORREF*,COLORREF*) = 0;
    virtual void turn_obj(double) = 0;;
    virtual void scan_node() = 0;
    virtual void ver_parties() = 0;
    virtual void get_info(INFO*) = 0;
    virtual void add_char(char*,int) = 0;
};

class Rect: public object
{
private:
    HRGN     rect;           //������ - �������������
    HRGN     node[9];
    COLORREF c1;
    COLORREF c2;
    bool     turn;
public:
    Rect(COLORREF*,COLORREF*);
    ~Rect();

    void draw(HDC*);                    //������� ��������� ��������������
    bool accessory(int,int);            //������� ���������� �������������� ������� ������� ������
    int accessory_bord(int,int);
    void select_transform(int,int,int); //���������������� ������
    void draw_transform(HDC*,bool);     //�������� ������ ��� �������������
    void move_obj(int,int);
    void fill_obj(COLORREF*,COLORREF*);
    void turn_obj(double);
    void scan_node();
    bool turn_or_not();
    void ver_parties();
    void get_info(INFO*);

    void add_char(char*,int){};
};

class ellipse: public object
{
private:
    HRGN    *node;
    HRGN     el;
    COLORREF c1;
    COLORREF c2;
public:
    ellipse(COLORREF*,COLORREF*);
    ~ellipse();

    void draw(HDC*);                    //������� ��������� �������
    bool accessory(int,int);            //������� ���������� �������������� ������� ������� ������
    int accessory_bord(int,int);
    void select_transform(int,int,int); //���������������� ������
    void draw_transform(HDC*,bool);     //�������� ������ ��� �������������
    void move_obj(int,int);
    void fill_obj(COLORREF*,COLORREF*);
    void scan_node();
    void get_info(INFO*);

    void turn_obj(double){};
    bool turn_or_not(){return 0;};
    void ver_parties(){};
    void add_char(char*,int){};
};

class FreeLine: public object //������������ �����
{
private:
    HRGN node;
    COLORREF c1;
public:
    /*
     � ����������� ��������� POINT ��� ������������,
     ������� �������� �� ������������ ��������� ���������� ������1 POINT(x, y),
     � ������� ���������� �� ��������� ��������� ���������� ����:
     points.push_back(POINT(x, y));
     ����� Point
    */
    FreeLine(COLORREF*);
    ~FreeLine();

    void draw(HDC*);                    //������� ��������� ������������ �����
    bool accessory(int,int);            //������� ���������� �������������� ������� ������� ������
    int  accessory_bord(int,int);
    void draw_transform(HDC*,bool);     //�������� ������ ��� �������������
    void move_obj(int,int);
    void fill_obj(COLORREF*,COLORREF*);
    void turn_obj(double);
    void scan_node();
    void get_info(INFO*);

    void select_transform(int,int,int){};//���������������� ������
    void ver_parties(){};
    void add_char(char*,int){};
};

class polygon: public object
{
private:
    HRGN node[2];
    COLORREF c1;
    COLORREF c2;
    HRGN    ps;
    double  alfa;
    HBRUSH  brush1;      //����� ��� �������
    HBRUSH  brush2;      //����� ��� �������
public:
    int     N;           //���-�� ������
    int     R;           //������ ��������� ����������
    polygon(COLORREF*,COLORREF*,int,int,int);
    ~polygon();

    void draw(HDC*);             //������� ��������� ��������������
    bool accessory(int,int);        //������� ���������� �������������� ������� ������� ������
    int  accessory_bord(int,int);
    void select_transform(int,int,int); //���������������� ������
    void draw_transform(HDC*,bool);   //�������� ������ ��� �������������
    void move_obj(int,int);
    void fill_obj(COLORREF*,COLORREF*);
    void scan_node();
    void turn_obj(double);
    void get_info(INFO*);

    void ver_parties(){};
    void add_char(char*,int){};
};

class Text: public object
{
private:
    RECT     rect;
    HFONT    hFont;
    COLORREF c1;
    COLORREF c2;
    string   st;
    int      N;           //���-�� ��������
public:
    Text(HWND*,COLORREF*,COLORREF*,int,int);
    ~Text();

    void draw(HDC*);                        //������� ���������
    bool accessory(int,int);                //������� ���������� �������������� ������� ������� ������
    void draw_transform(HDC*,bool);         //�������� ������ ��� �������������
    void move_obj(int,int);
    void fill_obj(COLORREF*,COLORREF*);
    void get_info(INFO*);
    void add_char(char*,int);

    int  accessory_bord(int,int){return 0;};
    void select_transform(int,int,int){};
    void scan_node(){};
    void turn_obj(double){};
    void ver_parties(){};
};
