#include "inform.h"

////////////////////// ������������� ///////////////////////////////
Rect::Rect(COLORREF* crCurUp,COLORREF* crCurDown)
{
    top=left=0;
    i=1;
    c1=*crCurUp;
    c2=*crCurDown;
    p=new POINT[5];
    vertex=new POINT[5];
}

Rect::~Rect()
{
    free(p);
    free(vertex);
    for(int i=0;i<9;i++)
            DeleteObject(node[i]);
}

void Rect::draw(HDC* hdcMemHolstDyn)
{
    HBRUSH  brush1;      //����� ��� �������
    HBRUSH  brush2;      //����� ��� �������
    brush1 = CreateSolidBrush(c1);
    brush2 = CreateSolidBrush(c2);
    DeleteObject(rect);
    rect=CreatePolygonRgn(p,5,WINDING);
    FillRgn(*hdcMemHolstDyn,rect,brush1);
    FrameRgn(*hdcMemHolstDyn,rect,brush2,2,2);
    DeleteObject(brush1);
    DeleteObject(brush2);
}

bool Rect::turn_or_not()
{
    turn=true;
    for(int i=0;i<5;i++)
        if (!((p[i].x==vertex[i].x)&&(p[i].y==vertex[i].y)))
        {
            turn=false;
            break;
        }
    return turn;
}

void Rect::ver_parties()
{
    int x,y;
    if (p[0].y>p[3].y)
    {
        top=true;
        y=p[0].y;
        p[0].y=p[4].y=vertex[0].y=vertex[4].y=p[3].y;
        p[3].y=vertex[3].y=y;
        y=p[1].y;
        p[1].y=vertex[1].y=p[2].y;
        p[2].y=vertex[2].y=y;
    }
    else
        top=false;
    if (p[0].x>p[2].x)
    {
        left=true;
        x=p[0].x;
        p[0].x=p[4].x=vertex[0].x=vertex[4].x=p[1].x;
        p[1].x=vertex[1].x=x;
        x=p[2].x;
        p[2].x=vertex[2].x=p[3].x;
        p[3].x=vertex[3].x=x;
    }
    else
        left=false;
}

void Rect::select_transform(int n, int dx, int dy)
{
    turn_or_not();
    if (turn)
        switch(n)
        {
            case 1:
                vertex[0].x=vertex[4].x=p[0].x=p[4].x=p[0].x-dx;
                vertex[0].y=vertex[4].y=p[0].y=p[4].y=p[0].y-dy;
                vertex[1].y=p[1].y-=dy;
                vertex[3].x=p[3].x-=dx;
                X=vertex[0].x+(vertex[1].x-vertex[0].x)/2.;
                Y=vertex[0].y+(vertex[3].y-vertex[0].y)/2.;
                break;
            case 2:
                vertex[0].y=p[0].y-=dy;
                vertex[1].y=p[1].y-=dy;
                vertex[4].y=p[4].y-=dy;
                Y=vertex[0].y+(vertex[3].y-vertex[0].y)/2.;
                break;
            case 3:
                vertex[1].x=p[1].x-=dx;
                vertex[1].y=p[1].y-=dy;
                vertex[0].y=vertex[4].y=p[0].y=p[4].y=p[0].y-dy;
                vertex[2].x=p[2].x-=dx;
                X=vertex[0].x+(vertex[1].x-vertex[0].x)/2.;
                Y=vertex[0].y+(vertex[3].y-vertex[0].y)/2.;
                break;
            case 4:
                vertex[1].x=p[1].x-=dx;
                vertex[2].x=p[2].x-=dx;
                X=vertex[0].x+(vertex[1].x-vertex[0].x)/2.;
                break;
            case 5:
                vertex[2].x=p[2].x-=dx;
                vertex[2].y=p[2].y-=dy;
                vertex[1].x=p[1].x-=dx;
                vertex[3].y=p[3].y-=dy;
                X=vertex[0].x+(vertex[1].x-vertex[0].x)/2.;
                Y=vertex[0].y+(vertex[3].y-vertex[0].y)/2.;
                break;
            case 6:
                vertex[2].y=p[2].y-=dy;
                vertex[3].y=p[3].y-=dy;
                Y=vertex[0].y+(vertex[3].y-vertex[0].y)/2.;
                break;
            case 7:
                vertex[3].x=p[3].x-=dx;
                vertex[3].y=p[3].y-=dy;
                vertex[2].y=p[2].y-=dy;
                vertex[0].x=vertex[4].x=p[0].x=p[4].x=p[0].x-dx;
                X=vertex[0].x+(vertex[1].x-vertex[0].x)/2.;
                Y=vertex[0].y+(vertex[3].y-vertex[0].y)/2.;
                /*//����� �������: 3D
                vertex[0].x=vertex[4].x=p[0].x=p[4].x=p[0].x-dx;
                vertex[0].y=vertex[4].y=p[0].y=p[4].y=p[0].y-dy;
                vertex[2].y=p[2].y-=dy;
                vertex[3].x=p[3].x-=dx;
                X=vertex[0].x+(vertex[1].x-vertex[0].x)/2.;
                Y=vertex[0].y+(vertex[3].y-vertex[0].y)/2.;*/
                break;
            case 8:
                vertex[0].x=p[0].x-=dx;
                vertex[3].x=p[3].x-=dx;
                vertex[4].x=p[4].x-=dx;
                X=vertex[0].x+(vertex[1].x-vertex[0].x)/2.;
                break;
        }
    scan_node();
}

void Rect::draw_transform(HDC* hdcMemHolstDyn,bool f)
{
    COLORREF c=RGB(0,0,0);
    COLORREF c2=RGB(0,255,0);
    HPEN pe=CreatePen(PS_DASH,0,c);

    SelectObject(*hdcMemHolstDyn,pe);
    Polyline(*hdcMemHolstDyn,&vertex[0],5);
    DeleteObject(pe);
    if (f)
    {
        HBRUSH brush1,brush2;
        if (f && turn_or_not())
        {
            brush1 = CreateSolidBrush(c);
            brush2 = CreateSolidBrush(c2);
            for(int i=0;i<9;i++)
            {
                FillRgn(*hdcMemHolstDyn,node[i],brush2);
                FrameRgn(*hdcMemHolstDyn,node[i],brush1,1,1);
            }
        }
        else
        {
            brush1 = CreateSolidBrush(c);
            brush2 = CreateSolidBrush(c2);
            FillRgn(*hdcMemHolstDyn,node[8],brush2);
            FrameRgn(*hdcMemHolstDyn,node[8],brush1,1,1);
        }
        DeleteObject(brush1);
        DeleteObject(brush2);
    }
}

void Rect::move_obj(int dx,int dy)
{
    for(int i=0;i<5;i++)
    {
        p[i].x=p[i].x-dx;
        p[i].y=p[i].y-dy;
        vertex[i].x=vertex[i].x-dx;
        vertex[i].y=vertex[i].y-dy;
    }
    X-=dx;
    Y-=dy;
    scan_node();
}

void Rect::fill_obj(COLORREF*crCurUp,COLORREF*crCurDown)
{
    c1=*crCurUp;
    c2=*crCurDown;
}

bool Rect::accessory(int x,int y)
{
    if (PtInRegion(rect,x,y))
            return 1;
    return 0;
}

int Rect::accessory_bord(int x,int y)
{
    if (PtInRegion(node[0],x,y))
        return 1;
    if (PtInRegion(node[1],x,y))
        return 2;
    if (PtInRegion(node[2],x,y))
        return 3;
    if (PtInRegion(node[3],x,y))
        return 4;
    if (PtInRegion(node[4],x,y))
        return 5;
    if (PtInRegion(node[5],x,y))
        return 6;
    if (PtInRegion(node[6],x,y))
        return 7;
    if (PtInRegion(node[7],x,y))
        return 8;
    if (PtInRegion(node[8],x,y))
        return 9;
    return 0;
}

void Rect::turn_obj(double  a)
{
    int min_x,max_x,min_y,max_y;
    int x,y,i,dx,dy;
    double rez,Xx=X,Yy=Y;
    for(i=0;i<5;i++)
    {
        x=p[i].x;
        y=p[i].y;
        dx=x - X;
        dy=y - Y;
        asm("fldl %[a];\n"      //�������� � ���� �
            "fsin;\n"           //��������� sin(a)
            "fild %[dy];\n"     //�������� � ���� dy
            "fmul;\n"           //�������� dy �� sin(a)
            "fldl %[a];\n"      //�������� � ���� �
            "fcos;\n"           //��������� cos(a)
            "fild %[dx];\n"     //�������� � ���� dx
            "fmul;\n"           //�������� dx �� cos(a)
            "fsub;\n"           //���������: dx*cos(a)-dy*sin(a)
            //"fldl %[Xx];\n"     //�������� � ���� X
            //"faddp;\n"          //����������
            "fstpl %[rez];\n"
            :[rez]"+m"(rez),[a]"+m"(a),[dy]"+m"(dy),[dx]"+m"(dx),[Xx]"+m"(Xx)::"memory");
        p[i].x=X+round(rez);
        asm("fldl %[a];\n"      //�������� � ���� �
            "fsin;\n"           //��������� sin(a)
            "fild %[dx];\n"     //�������� � ���� dx
            "fmul;\n"           //�������� dx �� sin(a)
            "fldl %[a];\n"      //�������� � ���� �
            "fcos;\n"           //��������� cos(a)
            "fild %[dy];\n"     //�������� � ���� dy
            "fmul;\n"           //�������� dy �� cos(a)
            "fadd;\n"           //���������: dy*cos(a)+dx*sin(a)
            //"fldl %[Yy];\n"     //�������� � ���� Y
            //"faddp;\n"          //����������
            "fstpl %[rez];\n"
            :[rez]"+m"(rez),[a]"+m"(a),[dy]"+m"(dy),[dx]"+m"(dx),[Yy]"+m"(Yy)::"memory");
        p[i].y=Y+round(rez);
        //p[i].x=X + round( dx * cos(a) - dy * sin(a) );
        //p[i].y=Y + round( dy * cos(a) + dx * sin(a));
    }
    max_x=min_x=p[0].x;
    max_y=min_y=p[0].y;
    for(i=0;i<5;i++)
    {
        if (p[i].x>max_x)
            max_x=p[i].x;
        else
            if (p[i].x<min_x)
                min_x=p[i].x;
        if (p[i].y>max_y)
            max_y=p[i].y;
        else
            if (p[i].y<min_y)
                min_y=p[i].y;
    }
    vertex[0].x=vertex[4].x=vertex[3].x=min_x;
    vertex[0].y=vertex[4].y=vertex[1].y=min_y;
    vertex[1].x=vertex[2].x=max_x;
    vertex[2].y=vertex[3].y=max_y;

    scan_node();
}

void Rect::scan_node()
{
    int i;
    bool f=true;
    for(i=0;i<5;i++)
        if (!((p[i].x==vertex[i].x)&&(p[i].y==vertex[i].y)))
        {
            f=false;
            break;
        }
    if (f)
    {
        for(int i=0;i<9;i++)
            DeleteObject(node[i]);
        node[0]=CreateEllipticRgn(p[0].x-6,p[0].y-6,p[0].x+6,p[0].y+6);
        node[1]=CreateEllipticRgn((p[0].x+(p[1].x-p[0].x)/2.)-6,(p[0].y+(p[1].y-p[0].y)/2.)-6,(p[0].x+(p[1].x-p[0].x)/2.)+6,(p[0].y+(p[1].y-p[0].y)/2.)+6);
        node[2]=CreateEllipticRgn(p[1].x-6,p[1].y-6,p[1].x+6,p[1].y+6);
        node[3]=CreateEllipticRgn((p[1].x+(p[2].x-p[1].x)/2.)-6,(p[1].y+(p[2].y-p[1].y)/2.)-6,(p[1].x+(p[2].x-p[1].x)/2.)+6,(p[1].y+(p[2].y-p[1].y)/2.)+6);
        node[4]=CreateEllipticRgn(p[2].x-6,p[2].y-6,p[2].x+6,p[2].y+6);
        node[5]=CreateEllipticRgn((p[2].x+(p[3].x-p[2].x)/2.)-6,(p[2].y+(p[3].y-p[2].y)/2.)-6,(p[2].x+(p[3].x-p[2].x)/2.)+6,(p[2].y+(p[3].y-p[2].y)/2.)+6);
        node[6]=CreateEllipticRgn(p[3].x-6,p[3].y-6,p[3].x+6,p[3].y+6);
        node[7]=CreateEllipticRgn((p[3].x+(p[4].x-p[3].x)/2.)-6,(p[3].y+(p[4].y-p[3].y)/2.)-6,(p[3].x+(p[4].x-p[3].x)/2.)+6,(p[3].y+(p[4].y-p[3].y)/2.)+6);
        node[8]=CreateEllipticRgn(X-6,Y-6,X+6,Y+6);
    }
    else
    {
        DeleteObject(node[8]);
        node[8]=CreateEllipticRgn(X-6,Y-6,X+6,Y+6);
    }
}

void Rect::get_info(INFO *inf)
{
    inf->i=i;
    inf->X=X;
    inf->Y=Y;
    int w,h;
    w=sqrt((p[1].x-p[0].x)*(p[1].x-p[0].x)+(p[1].y-p[0].y)*(p[1].y-p[0].y));
    h=sqrt((p[2].x-p[1].x)*(p[2].x-p[1].x)+(p[2].y-p[1].y)*(p[2].y-p[1].y));
    if (w>0)
        inf->W=w;
    else
        inf->W=-w;
    if (h>0)
        inf->H=h;
    else
        inf->H=-h;
    inf->c1=c1;
    inf->c2=c2;
}

////////////////////// ������ ///////////////////////////////
ellipse::~ellipse()
{
    free(p);
    for(int i=0;i<8;i++)
            DeleteObject(node[i]);
    free(node);
}

ellipse::ellipse(COLORREF* crCurUp,COLORREF* crCurDown)
{
    p=new POINT[5];
    node=new HRGN[8];
    i=2;
    c1=*crCurUp;
    c2=*crCurDown;
}

void ellipse::draw(HDC* hdcMemHolstDyn)
{
    HBRUSH  brush1;      //����� ��� �������
    HBRUSH  brush2;      //����� ��� �������
    brush1 = CreateSolidBrush(c1);
    brush2 = CreateSolidBrush(c2);
    DeleteObject(el);
    el=CreateEllipticRgn(p[3].x, p[4].y, p[1].x, p[2].y);
    FillRgn(*hdcMemHolstDyn,el,brush1);
    FrameRgn(*hdcMemHolstDyn,el,brush2,2,2);
    DeleteObject(brush1);
    DeleteObject(brush2);
}

void ellipse::draw_transform(HDC* hdcMemHolstDyn,bool f)
{
    scan_node();
    COLORREF c=RGB(0,0,0);
    COLORREF c2=RGB(0,255,0);
    HPEN pe=CreatePen(PS_DASH,0,c);

    SelectObject(*hdcMemHolstDyn,pe);
    Polyline(*hdcMemHolstDyn,&p[0],5);

    if (f)
    {
        HBRUSH brush1,brush2;

        brush1 = CreateSolidBrush(c);
        brush2 = CreateSolidBrush(c2);
        for(int i=0;i<8;i++)
        {
            FillRgn(*hdcMemHolstDyn,node[i],brush2);
            FrameRgn(*hdcMemHolstDyn,node[i],brush1,1,1);
        }

        DeleteObject(brush1);
        DeleteObject(brush2);
    }
    DeleteObject(pe);
}

bool ellipse::accessory(int x,int y)
{
    if (PtInRegion(el,x,y))
            return 1;
    return 0;
}

int ellipse::accessory_bord(int x,int y)
{
    if (PtInRegion(node[0],x,y))
        return 1;
    if (PtInRegion(node[1],x,y))
        return 2;
    if (PtInRegion(node[2],x,y))
        return 3;
    if (PtInRegion(node[3],x,y))
        return 4;
    if (PtInRegion(node[4],x,y))
        return 5;
    if (PtInRegion(node[5],x,y))
        return 6;
    if (PtInRegion(node[6],x,y))
        return 7;
    if (PtInRegion(node[7],x,y))
        return 8;
    return 0;
}

void ellipse::move_obj(int dx,int dy)
{
    for(int i=0;i<5;i++)
    {
        p[i].x-=dx;
        p[i].y-=dy;
    }
    X-=dx;
    Y-=dy;
}

void ellipse::fill_obj(COLORREF*crCurUp,COLORREF*crCurDown)
{
    c1=*crCurUp;
    c2=*crCurDown;
}

void ellipse::select_transform(int n, int dx, int dy)
{
    int min_x,max_x,min_y,max_y;
    switch(n)
    {
        case 1:
            p[0].x=p[4].x=p[0].x-dx;
            p[0].y=p[4].y=p[0].y-dy;
            p[1].y-=dy;
            p[3].x-=dx;
            break;
        case 2:
            p[0].y=p[4].y=p[0].y-dy;
            p[1].y-=dy;
            break;
        case 3:
            p[1].x-=dx;
            p[1].y-=dy;
            p[0].y=p[4].y=p[0].y-dy;
            p[2].x-=dx;
            break;
        case 4:
            p[1].x-=dx;
            p[2].x-=dx;
            break;
        case 5:
            p[2].x-=dx;
            p[2].y-=dy;
            p[1].x-=dx;
            p[3].y-=dy;
            break;
        case 6:
            p[2].y-=dy;
            p[3].y-=dy;
            break;
        case 7:
            p[3].x-=dx;
            p[3].y-=dy;
            p[0].x=p[4].x=p[0].x-dx;
            p[2].y-=dy;
            break;
        case 8:
            p[0].x=p[4].x=p[0].x-dx;
            p[3].x-=dx;
            break;
    }
    max_x=min_x=p[0].x;
    max_y=min_y=p[0].y;
    for(int i=0;i<5;i++)
    {
        if (p[i].x>max_x)
            max_x=p[i].x;
        else
            if (p[i].x<min_x)
                min_x=p[i].x;
        if (p[i].y>max_y)
            max_y=p[i].y;
        else
            if (p[i].y<min_y)
                min_y=p[i].y;
    }
    X=min_x+(max_x-min_x)/2.;
    Y=min_y+(max_y-min_y)/2.;
}

void ellipse::scan_node()
{
    int a,b,c,d;
    a=p[0].x;
    b=p[0].y;
    c=p[2].x;
    d=p[2].y;
    for(int i=0;i<8;i++)
            DeleteObject(node[i]);
    node[0]=CreateEllipticRgn(a-6,b-6,a+6,b+6);
    node[1]=CreateEllipticRgn(a+(c-a)/2.-6,b-6,a+(c-a)/2.+6,b+6);
    node[2]=CreateEllipticRgn(c-6,b-6,c+6,b+6);
    node[3]=CreateEllipticRgn(c-6,b+(d-b)/2.-6,c+6,b+(d-b)/2.+6);
    node[4]=CreateEllipticRgn(c-6,d-6,c+6,d+6);
    node[5]=CreateEllipticRgn(a+(c-a)/2.-6,d-6,a+(c-a)/2.+6,d+6);
    node[6]=CreateEllipticRgn(a-6,d-6,a+6,d+6);
    node[7]=CreateEllipticRgn(a-6,b+(d-b)/2.-6,a+6,b+(d-b)/2.+6);
}

void ellipse::get_info(INFO *inf)
{
    inf->i=i;
    inf->X=X;
    inf->Y=Y;
    int r1,r2;
    r1=round((p[1].x-p[0].x)/2.);
    r2=round((p[2].y-p[1].y)/2.);
    if (r1>0)
        inf->R1=r1;
    else
        inf->R1=-r1;
    if (r2>0)
        inf->R2=r2;
    else
        inf->R2=-r2;
    inf->c1=c1;
    inf->c2=c2;
}

////////////////////// ������������ ����� ///////////////////////////////
FreeLine::FreeLine(COLORREF* crCurUp)
{
    c1=*crCurUp;
    p=new POINT[5];
    i=4;
}

FreeLine::~FreeLine()
{
    points.clear();
    free(p);
    DeleteObject(node);
}

void FreeLine::draw(HDC* hdcMemHolstDyn)
{
    HPEN pe=CreatePen(PS_SOLID,0,c1);
    SelectObject(*hdcMemHolstDyn,pe);
    for(unsigned int i=0;i<points.size();i++)
    {
        it = points.begin();
        MoveToEx(*hdcMemHolstDyn, it->x, it->y, NULL);
        for (it + 1; it != points.end();++it)
            LineTo(*hdcMemHolstDyn, it->x, it->y);
    }
}

bool FreeLine::accessory(int x,int y)
{
    for(unsigned int i=0;i<points.size();i++)
    {
        if ((points[i].x<=x+5 && points[i].x>=x-5) && (points[i].y<=y+5 && points[i].y>=y-5))
            return 1;
    }
    return 0;
}

int FreeLine::accessory_bord(int x,int y)
{
    if (PtInRegion(node,x,y))
        return 9;
    return 0;
}

void FreeLine::draw_transform(HDC* hdcMemHolstDyn,bool f)
{
    scan_node();
    COLORREF c=RGB(0,0,0);
    COLORREF c2=RGB(0,255,0);
    HPEN pe=CreatePen(PS_DASH,0,c);

    SelectObject(*hdcMemHolstDyn,pe);
    Polyline(*hdcMemHolstDyn,&p[0],5);
    DeleteObject(pe);

    if (f)
    {
        HBRUSH brush1,brush2;

        brush1 = CreateSolidBrush(c);
        brush2 = CreateSolidBrush(c2);
        {
            FillRgn(*hdcMemHolstDyn,node,brush2);
            FrameRgn(*hdcMemHolstDyn,node,brush1,1,1);
        }

        DeleteObject(brush1);
        DeleteObject(brush2);
    }
}

void FreeLine::move_obj(int dx,int dy)
{
    unsigned int i;
    for(i=0;i<points.size();i++)
    {
        points[i].x=points[i].x-dx;
        points[i].y=points[i].y-dy;
    }
    for(i=0;i<5;i++)
    {
        p[i].x=p[i].x-dx;
        p[i].y=p[i].y-dy;
    }
    X-=dx;
    Y-=dy;
    scan_node();
}

void FreeLine::fill_obj(COLORREF*crCurUp,COLORREF*crCurDown)
{
    c1=*crCurUp;
}

void FreeLine::turn_obj(double  a)
{
    unsigned int i;
    int x,y,X,Y;
    double rez,Xx,Yy;
    int dx,dy;
    int max_x=-1, min_x=INFINITE, max_y=-1, min_y=INFINITE;
    X=p[0].x+(p[2].x-p[0].x)/2.,
    Y=p[0].y+(p[2].y-p[0].y)/2.;
    Xx=X;
    Yy=Y;
    for(i=0;i<points.size();i++)
    {
        x=points[i].x;
        y=points[i].y;
        dx=x - X;
        dy=y - Y;
        asm("fldl %[a];\n"      //�������� � ���� �
            "fsin;\n"           //��������� sin(a)
            "fild %[dy];\n"     //�������� � ���� dy
            "fmul;\n"           //�������� dy �� sin(a)
            "fldl %[a];\n"      //�������� � ���� �
            "fcos;\n"           //��������� cos(a)
            "fild %[dx];\n"     //�������� � ���� dx
            "fmul;\n"           //�������� dx �� cos(a)
            "fsub;\n"           //���������: dx*cos(a)-dy*sin(a)
            //"fldl %[Xx];\n"     //�������� � ���� X
            //"faddp;\n"          //����������
            "fstpl %[rez];\n"
            :[rez]"+m"(rez),[a]"+m"(a),[dy]"+m"(dy),[dx]"+m"(dx),[Xx]"+m"(Xx)::"memory");
        points[i].x=X+round(rez);
        asm("fldl %[a];\n"      //�������� � ���� �
            "fsin;\n"           //��������� sin(a)
            "fild %[dx];\n"     //�������� � ���� dx
            "fmul;\n"           //�������� dx �� sin(a)
            "fldl %[a];\n"      //�������� � ���� �
            "fcos;\n"           //��������� cos(a)
            "fild %[dy];\n"     //�������� � ���� dy
            "fmul;\n"           //�������� dy �� cos(a)
            "fadd;\n"           //���������: dy*cos(a)+dx*sin(a)
            //"fldl %[Yy];\n"     //�������� � ���� Y
            //"faddp;\n"          //����������
            "fstpl %[rez];\n"
            :[rez]"+m"(rez),[a]"+m"(a),[dy]"+m"(dy),[dx]"+m"(dx),[Yy]"+m"(Yy)::"memory");
        points[i].y=Y+round(rez);
        //points[i].x=X + round((x - X) * cos(a) - (y - Y) * sin(a));
        //points[i].y=Y + round((y - Y) * cos(a) + (x - X) * sin(a));
    }
    max_x=min_x=points[0].x;
    max_y=min_y=points[0].y;
    for(i=0;i<points.size();i++)
    {
        if (points[i].x>max_x)
            max_x=points[i].x;
        else
            if (points[i].x<min_x)
                min_x=points[i].x;
        if (points[i].y>max_y)
            max_y=points[i].y;
        else
            if (points[i].y<min_y)
                min_y=points[i].y;
    }
    p[0].y=p[1].y=min_y;
    p[1].x=p[2].x=max_x;
    p[2].y=p[3].y=max_y;
    p[3].x=p[0].x=min_x;
    p[4].x=p[0].x=min_x;
    p[4].y=p[0].y=min_y;

    scan_node();
}

void FreeLine::scan_node()
{
    DeleteObject(node);
    node=CreateEllipticRgn(p[0].x+(p[2].x-p[0].x)/2.-6,p[0].y+(p[2].y-p[0].y)/2.-6,p[0].x+(p[2].x-p[0].x)/2.+6,p[0].y+(p[2].y-p[0].y)/2.+6);
}

void FreeLine::get_info(INFO *inf)
{
    inf->i=4;
    inf->X=X;
    inf->Y=Y;
    inf->c1=c1;
    inf->N=points.size();
}

////////////////////// ������������� ///////////////////////////////
polygon::polygon(COLORREF* crCurUp,COLORREF* crCurDown,int n,int x,int y)
{
    p=new POINT[n+1];
    vertex=new POINT[5];
    N=n;
    X=x;
    Y=y;
    i=3;
    alfa=0;
    c1=*crCurUp;
    c2=*crCurDown;
    brush1 = CreateSolidBrush(c1);
    brush2 = CreateSolidBrush(c2);
}

polygon::~polygon()
{
    free(p);
    free(vertex);
    DeleteObject(node[0]);
    DeleteObject(node[1]);
    DeleteObject(brush1);
    DeleteObject(brush2);
}

void polygon::draw(HDC* hdcMemHolstDyn)
{
    DeleteObject(ps);
    ps=CreatePolygonRgn(p,N+1,WINDING);
    FillRgn(*hdcMemHolstDyn,ps,brush1);
    FrameRgn(*hdcMemHolstDyn,ps,brush2,2,2);
}

void polygon::draw_transform(HDC* hdcMemHolstDyn,bool f)
{
    COLORREF c=RGB(0,0,0);
    COLORREF c2=RGB(0,255,0);
    HPEN pe=CreatePen(PS_DASH,0,c);

    SelectObject(*hdcMemHolstDyn,pe);
    Polyline(*hdcMemHolstDyn,&vertex[0],5);
    DeleteObject(pe);

    if (f)
    {
        HBRUSH brush1,brush2;

        brush1 = CreateSolidBrush(c);
        brush2 = CreateSolidBrush(c2);
        for(int i=0;i<2;i++)
        {
            FillRgn(*hdcMemHolstDyn,node[i],brush2);
            FrameRgn(*hdcMemHolstDyn,node[i],brush1,1,1);
        }
        DeleteObject(brush1);
        DeleteObject(brush2);
    }
    DeleteObject(pe);
}

bool polygon::accessory(int x,int y)
{
    if (PtInRegion(ps,x,y))
            return 1;
    return 0;
}

int polygon::accessory_bord(int x,int y)
{
    if (PtInRegion(node[0],x,y))
        return 1;
    if (PtInRegion(node[1],x,y))
        return 9;
    return 0;
}

void polygon::move_obj(int dx,int dy)
{
    for(int i=0;i<N+1;i++)
    {
        p[i].x-=dx;
        p[i].y-=dy;
    }
    for(int i=0;i<5;i++)
    {
        vertex[i].x-=dx;
        vertex[i].y-=dy;
    }
    X-=dx;
    Y-=dy;
    scan_node();
}

void polygon::fill_obj(COLORREF*crCurUp,COLORREF*crCurDown)
{
    DeleteObject(brush1);
    DeleteObject(brush2);
    brush1 = CreateSolidBrush(*crCurUp);
    brush2 = CreateSolidBrush(*crCurDown);
    c1=*crCurUp;
    c2=*crCurDown;
}

void polygon::turn_obj(double  a)
{
    int min_x,max_x,min_y,max_y;

    alfa+=a;
    if (alfa>0 && alfa>=6.2832)
        alfa-=6.2832;
    if (alfa<0 && alfa<=6.2832)
        alfa+=6.2832;
    a=a*180/3.14;
    int i,j;
    for (j=0,i=1;i<N*2;i++)
    {
        if (i%2)
        {
            p[j].x=X+R*cos(a*3.14/180);
            p[j].y=Y-R*sin(a*3.14/180);
            j++;
        }
        a=a+180/N;
    }
    p[j].x=p[0].x;
    p[j].y=p[0].y;

    max_x=min_x=p[0].x;
    max_y=min_y=p[0].y;
    for(i=0;i<N+1;i++)
    {
        if (p[i].x>max_x)
            max_x=p[i].x;
        else
            if (p[i].x<min_x)
                min_x=p[i].x;
        if (p[i].y>max_y)
            max_y=p[i].y;
        else
            if (p[i].y<min_y)
                min_y=p[i].y;
    }
    vertex[0].x=vertex[4].x=vertex[3].x=min_x;
    vertex[0].y=vertex[4].y=vertex[1].y=min_y;
    vertex[1].x=vertex[2].x=max_x;
    vertex[2].y=vertex[3].y=max_y;

    scan_node();
}

void polygon::select_transform(int n, int dx, int dy)
{
    R=round(sqrt((X-dx)*(X-dx)+(Y-dy)*(Y-dy)));
    turn_obj(0);
}

void polygon::scan_node()
{
    DeleteObject(node[0]);
    DeleteObject(node[1]);
    node[0]=CreateEllipticRgn(p[0].x-6,p[0].y-6,p[0].x+6,p[0].y+6);
    node[1]=CreateEllipticRgn(X-6,Y-6,X+6,Y+6);
}

void polygon::get_info(INFO *inf)
{
    inf->i=i;
    inf->X=X;
    inf->Y=Y;
    inf->N=N;
    inf->R1=R;
    inf->c1=c1;
    inf->c2=c2;
}

/////////////////////////////////// ����� ///////////////////////////////////////////////////
Text::Text(HWND *hWnd,COLORREF* crCurUp,COLORREF* crCurDown,int x, int y)
{
    c1=*crCurUp;
    c2=*crCurDown;
    st="�����";
    N=st.length();
    X=x;
    Y=y;
    i=6;

    char name[]="resources//LOGFONT";
    ifstream  in(name,ios::binary|ios::in);
    in.read((char*)&lf,sizeof lf);
    in.read((char*)&cf.iPointSize,sizeof cf.iPointSize);
    in.close();

    cf.Flags=CF_EFFECTS|CF_INITTOLOGFONTSTRUCT|CF_SCREENFONTS|CF_FORCEFONTEXIST;
    cf.hwndOwner=*hWnd;
    cf.lpLogFont=&lf;
    cf.lStructSize=sizeof(CHOOSEFONT);
    cf.rgbColors=c1;\
}

void Text::draw(HDC* hdcMemHolstDyn)
{
    N=st.length();

    char str[N];
    c1=cf.rgbColors;
    for(int i=0;i<N;i++)
        str[i]=st[i];
    rect.left=X;
    rect.top=Y;
    SetMapMode(*hdcMemHolstDyn, MM_TEXT);
    hFont=CreateFontIndirect(&lf);
    SelectObject(*hdcMemHolstDyn, hFont);
    SetBkColor(*hdcMemHolstDyn,c2);
    SetTextColor(*hdcMemHolstDyn, cf.rgbColors);

    DrawText(*hdcMemHolstDyn,str,st.length(),&rect, DT_LEFT | DT_CALCRECT);
    //lets try to draw
    DrawText(*hdcMemHolstDyn,str,st.length(),&rect,DT_NOCLIP);
}

void Text::draw_transform(HDC* hdcMemHolstDyn,bool f)
{
    COLORREF c=RGB(0,0,0);
    HPEN pe=CreatePen(PS_DASH,0,c);
    SelectObject(*hdcMemHolstDyn,(HBRUSH)NULL_BRUSH);
    SelectObject(*hdcMemHolstDyn,pe);
    Rectangle(*hdcMemHolstDyn,rect.left-2,rect.top-2,rect.right+2,rect.bottom+2);
    DeleteObject(pe);
}

bool Text::accessory(int x,int y)
{
    POINT pt;
    pt.x=x;
    pt.y=y;
    if (PtInRect(&rect,pt))
            return 1;
    return 0;
}

void Text::move_obj(int dx,int dy)
{
    rect.left-=dx;
    rect.top-=dy;
    rect.right-=dx;
    rect.bottom-=dy;
    X-=dx;
    Y-=dy;
}

void Text::fill_obj(COLORREF*crCurUp,COLORREF*crCurDown)
{
    c1=*crCurUp;
    cf.rgbColors=c1;
    c2=*crCurDown;
}

void Text::get_info(INFO* inf)
{
    inf->i=6;
    inf->X=X;
    inf->Y=Y;
    inf->N=N;
    inf->st=st;
    inf->c1=c1;
    inf->c2=c2;
    inf->rect=rect;
}

void Text::add_char(char* buf,int num)
{
    st.clear();
    st.append(buf);
    N=num;
}
