#include "menu.h"
#include "inform.h"
#include <windowsx.h>
#include <iostream>
#include <shellapi.h> //��� �������� �����

using namespace std;

LRESULT CALLBACK WndMain (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK AboutProc (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK ListObjectProc (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK ObjectPropProc (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndPalette (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndHolst (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndList (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndLayer (HWND, UINT, WPARAM, LPARAM);

LRESULT CALLBACK WndB0 (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndB3 (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndB4 (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndB5 (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndB6 (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndB8 (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndB9 (HWND, UINT, WPARAM, LPARAM);

LRESULT CALLBACK WndB_del (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndB_up (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndB_down (HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK WndB_enter (HWND, UINT, WPARAM, LPARAM);

int  RisPal();
void RepaintBack();
void save_as(char *);
void save_svg(char *);
void open_file(char *);
void fill_list();

vector   <object*> shapes;              //��� ������ �������!
object   *temp=NULL;                    //��������� �������������, ���������� ������� ����
FreeLine *el_FreeLine;
Rect     *el_rect;
ellipse  *el_ellipse;
polygon  *el_polygon;
Text     *el_Text;
HINSTANCE	hInst;
// ����������� ����
HWND		hwndMain,hwndAbout,hwndPalette,hwndList,hwndLayer,hwndHolst;
HWND        hwnd_list,hwnd_obj_propert;
HWND        hwB0,hwB1,hwB3,hwB4,hwB5,hwB6,hwB8,hwB9;
HWND        hwBdel,hwBup,hwBdown;
HWND        hwndEdit_Y,hwndEdit_X,hwndEdit_R1,hwndEdit_R2,hwndEdit_width,hwndEdit_height,hwndEdit_num,hwndEdit_text,hwndBut_enter;
HWND        tx1,tx2,tx3,tx4,tx5,tx6,tx7;

char		szAppName[260]	= "��������� ����������� ��������";									    // �������� ����������
char		szBuf[256],File[260],TitleFile[260],File2[260],TitleFile2[260],WinText[260];			// ����� ��� ���������� �������������
int			status=1,i,j;											    // ������ �������,i,j
int			tekI=0,tekJ=0;												// ������� ������ �����
int         numBord=0;                                                  // ������� ����������

COLORREF    crPalCol[14][2];											// ������� ������ �������
COLORREF	crCurUp;
COLORREF	crCurDown;													// ��� ����� - ������� � ������
WNDPROC		OldWndList;													// ����� ������������� ���������

HDC			hdcMemPalette,hdcMemHolstBack,hdcMemHolstDyn;				// ��������� ������
HDC			hdcPalette,hdcHolst;										// ��������� �������
HPEN		hSelectPen,hPen,hPenD,hPenL;								// �����
HBRUSH		hSelectBrush,hBrush;										// �����

BOOL		IsUpColor,bTracking=0,ed_text=0;
BOOL        selected=0;                                                 //������ �������
int         id=-1;                                                      //������ ����������� ������� ��� ��������
char        bufRect[]    ="Rectangle";
char        bufEllipse[] ="Ellipse";
char        bufPolygon[] ="Polygon";
char        bufFreehand[]="Freehand line";
char        bufText[]    ="Text";

OPENFILENAME svld,svld1;

int WINAPI WinMain (HINSTANCE hThisInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpszArgument,
                     int nCmdShow)
{
    MSG			msg;
	WNDCLASSEX	wndclass;
	hInst = hThisInstance;

	wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_DBLCLKS |CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndMain;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst, "IDB_ICO");
	wndclass.hIconSm		= LoadIcon(hInst, "IDB_ICO");
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);//COLOR_BTNFACE
	wndclass.lpszMenuName	= "Main";
	wndclass.lpszClassName	= szAppName;
	if (!RegisterClassEx(&wndclass))
        return 0;

    ZeroMemory(&wndclass,sizeof(WNDCLASSEX));
    wndclass.cbSize         = sizeof(WNDCLASSEX);
    wndclass.lpfnWndProc    = AboutProc;
    wndclass.hInstance      = hInst;
    wndclass.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);//
    wndclass.lpszClassName  = "about";
    if (!RegisterClassEx(&wndclass))
        return 0;

    ZeroMemory(&wndclass,sizeof(WNDCLASSEX));
    wndclass.cbSize         = sizeof(WNDCLASSEX);
    wndclass.lpfnWndProc    = ListObjectProc;
    wndclass.hInstance      = hInst;
    wndclass.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);//
    wndclass.lpszClassName  = "list_obj";
    if (!RegisterClassEx(&wndclass))
        return 0;

    ZeroMemory(&wndclass, sizeof(WNDCLASSEX));
    wndclass.cbSize         = sizeof(WNDCLASSEX);
    wndclass.lpfnWndProc    = ObjectPropProc;
    wndclass.hInstance      = hInst;
    wndclass.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wndclass.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wndclass.lpszClassName  = "obj_prop";
    if (!RegisterClassEx(&wndclass))
        return 0;

	wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndPalette;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "PaletteClass";
	if (!RegisterClassEx(&wndclass))
        return 0;

	wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndHolst;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_WINDOW+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "HolstClass";
	if (!RegisterClassEx(&wndclass))
        return 0;

    wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndLayer;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "LayerClass";
	if (!RegisterClassEx(&wndclass))
        return 0;

    wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndB0;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "B0";
	if (!RegisterClassEx(&wndclass))
        return 0;


    wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndB3;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "B3";
	if (!RegisterClassEx(&wndclass))
        return 0;
    wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndB4;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "B4";
	if (!RegisterClassEx(&wndclass))
        return 0;
    wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndB5;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "B5";
	if (!RegisterClassEx(&wndclass))
        return 0;
    wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndB6;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "B6";
	if (!RegisterClassEx(&wndclass))
        return 0;
    wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndB8;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "B8";
	if (!RegisterClassEx(&wndclass))
        return 0;
    wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndB9;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "B9";
	if (!RegisterClassEx(&wndclass))
        return 0;
    wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndB_del;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "B_del";
	if (!RegisterClassEx(&wndclass))
        return 0;
    wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndB_up;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "B_up";
	if (!RegisterClassEx(&wndclass))
        return 0;
    wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndB_down;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "B_down";
	if (!RegisterClassEx(&wndclass))
        return 0;
    wndclass.cbSize			= sizeof(WNDCLASSEX);
	wndclass.style			= CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc	= WndB_enter;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hInst;
	wndclass.hCursor		= LoadCursor(NULL,IDC_ARROW);
	wndclass.hIcon			= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hIconSm		= LoadIcon(hInst,IDI_APPLICATION);
	wndclass.hbrBackground	= (HBRUSH) (COLOR_BTNFACE+1);
	wndclass.lpszMenuName	= 0;
	wndclass.lpszClassName	= "B_enter";
	if (!RegisterClassEx(&wndclass))
        return 0;

    hwndMain = CreateWindowEx (
               0,                   /* Extended possibilites for variation */
               szAppName,           /* Classname */
               "��������� ����������� ��������",               /* Title Text */
               WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|MDIS_ALLCHILDSTYLES, /* default window */
               CW_USEDEFAULT,       /* Windows decides the position */
               CW_USEDEFAULT,       /* where the window ends up on the screen */
               CW_USEDEFAULT,                 /* The programs width */
               CW_USEDEFAULT,                 /* and height in pixels */
               HWND_DESKTOP,        /* The window is a child-window to desktop */
               NULL,                /* No menu */
               hThisInstance,       /* Program Instance handler */
               NULL                 /* No Window Creation data */
               );
    ShowWindow (hwndMain, SW_SHOWMAXIMIZED);
    UpdateWindow(hwndMain);

    while (1)
    {
        if (PeekMessage(&msg,NULL,0,0, PM_REMOVE))
        {
            if(msg.message==WM_QUIT)
                break;
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
    /*while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }*/
    return msg.wParam;
}

LRESULT CALLBACK WndMain (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
	// ���������� ����������
	RECT			rect;
	// ���������� ���������
	HFONT			hFont;		// ����� ��� ListBox'�
    // ���������� ��������� �������
	static COLORREF	crCustColor[16];	// ������� ������ ��� ���. �������
	static CHOOSECOLOR	cc;				// ��������� ��� ���. �������

    // ������ ����������
	HBITMAP			hBitmap;// ��������

    ofstream fout;

    switch (message)
    {
        case WM_CREATE:
        {
            memset(&svld, 0, sizeof(OPENFILENAME));
            memset(&File, '\0', sizeof(File));
            memset(&TitleFile, '\0', sizeof(TitleFile));
            svld.lStructSize     = sizeof(svld);
            svld.lpstrFile       = File;
            svld.nMaxFile        = sizeof(File);
            svld.lpstrFilter     = "*.SVG";
            svld.nFilterIndex    = 1;
            svld.lpstrFileTitle  = TitleFile;
            svld.nMaxFileTitle   = sizeof(TitleFile);
            svld.lpstrInitialDir = NULL;
            svld.Flags           = OFN_EXPLORER|OFN_NOCHANGEDIR|OFN_OVERWRITEPROMPT|OFN_PATHMUSTEXIST ;

            memset(&svld1, 0, sizeof(OPENFILENAME));
            memset(&File2, '\0', sizeof(File2));
            memset(&TitleFile2, '\0', sizeof(TitleFile2));
            svld1.lStructSize     = sizeof(svld1);
            svld1.lpstrFile       = File2;
            svld1.nMaxFile        = sizeof(File2);
            svld1.lpstrFilter     = "(*.VG)\0*.VG\0��� �����(*.*)\0*.*\0\0";
            svld1.nFilterIndex    = 1;
            svld1.lpstrFileTitle  = TitleFile2;
            svld1.nMaxFileTitle   = sizeof(TitleFile2);
            svld1.lpstrInitialDir = NULL;
            svld1.Flags           = OFN_EXPLORER|OFN_NOCHANGEDIR|OFN_OVERWRITEPROMPT|OFN_PATHMUSTEXIST ;

            GetWindowRect(hwnd,&rect);
            hwnd_list=CreateWindowEx(
                                    0,
                                    "list_obj",
                                    "",
                                    WS_CHILD|WS_DLGFRAME|WS_GROUP ,
                                    0,
                                    0,
                                    259,
                                    36,
                                    hwnd,
                                    NULL,
                                    hInst,
                                    NULL);
            hwndPalette=CreateWindowEx(
                                    WS_EX_WINDOWEDGE,
                                    "PaletteClass",
                                    "�������",
                                    WS_CHILD|WS_DLGFRAME|WS_VISIBLE,
                                    0,
                                    36,
                                    259,
                                    37,
                                    hwnd,
                                    NULL,
                                    hInst,
                                    NULL);

            hwnd_obj_propert=CreateWindowEx(
                                    0,
                                    "obj_prop",
                                    "",
                                    WS_DLGFRAME|WS_CHILD|WS_VISIBLE,
                                    0,
                                    72,
                                    259,
                                    220,
                                    hwnd,
                                    NULL,
                                    hInst,
                                    NULL);

            hwndLayer=CreateWindowEx(
                                    WS_EX_WINDOWEDGE,
                                    "LayerClass",
                                    "������ �� ������",
                                    WS_CHILD|WS_DLGFRAME,
                                    0,
                                    221+70,
                                    259,
                                    36,
                                    hwnd,
                                    NULL,
                                    hInst,
                                    NULL);

            ////////////////////// ������� ������ //////////////////////////
            hwndHolst	= CreateWindowEx(
                                    WS_EX_WINDOWEDGE,//WS_EX_CLIENTEDGE,
                                    "HolstClass",
                                    "�����",
                                    WS_CHILD|WS_DLGFRAME|WS_VISIBLE,//|WS_HSCROLL|WS_VSCROLL,
                                    260,
                                    1,
                                    800,//Horres-281,
                                    600,//Vertres-68,
                                    hwnd,
                                    NULL,
                                    hInst,
                                    NULL);
            ///////////////////// ������� �������� //////////////////////
            hwndList = CreateWindowEx(
                                    WS_EX_CLIENTEDGE,
                                    "listbox",
                                    "�������",
                                    WS_CHILD | WS_VISIBLE | WS_VSCROLL | LBS_DISABLENOSCROLL | LBS_NOTIFY | LBS_NOINTEGRALHEIGHT,
                                    0,
                                    221+70+36,
                                    260,
                                    274,
                                    hwnd,
                                    NULL,
                                    hInst,
                                    NULL);
            hFont = CreateFont(
                                18,
                                0,
                                -900,
                                -900,
                                FW_NORMAL,
                                FALSE,
                                FALSE,
                                FALSE,
                                DEFAULT_CHARSET,
                                OUT_DEFAULT_PRECIS,
                                CLIP_DEFAULT_PRECIS,
                                DEFAULT_QUALITY,
                                DEFAULT_PITCH | FF_DONTCARE,
                                "Terminal"
                                );
            SendMessage(hwndList,WM_SETFONT,(WPARAM)hFont,0);
            OldWndList = (WNDPROC) SetWindowLong (hwndList,GWL_WNDPROC, (LONG) WndList);

            hBitmap = LoadBitmap(hInst,"IDB_PALETTE");				// �������������� �������� �������
            hdcMemPalette = CreateCompatibleDC(GetDC(hwndPalette));	// ������� ������ �������
            SelectObject(hdcMemPalette, hBitmap);					// ������� � �������� ������ ��������

            /////////////////////// ������������� ������ ������� ������� ////
            cc.lStructSize		= sizeof(CHOOSECOLOR);
            cc.hwndOwner		= hwnd;
            cc.hInstance		= NULL;
            cc.rgbResult		= RGB(0x80, 0x80, 0x80);
            cc.lpCustColors		= crCustColor;
            cc.Flags			= CC_RGBINIT | CC_FULLOPEN;
            cc.lCustData		= NULL;
            cc.lpfnHook			= NULL;
            cc.lpTemplateName	= NULL;

            ////////////////////// ������������� ������ ������� /////////////
            crPalCol[0][0]=RGB(0,0,0);
            crPalCol[1][0]=RGB(128,128,128);
            crPalCol[2][0]=RGB(128,0,0);
            crPalCol[3][0]=RGB(128,128,0);
            crPalCol[4][0]=RGB(0,128,0);
            crPalCol[5][0]=RGB(0,128,128);
            crPalCol[6][0]=RGB(0,0,128);
            crPalCol[7][0]=RGB(128,0,128);
            crPalCol[8][0]=RGB(128,128,64);
            crPalCol[9][0]=RGB(0,0,64);
            crPalCol[10][0]=RGB(0,128,255);
            crPalCol[11][0]=RGB(0,64,128);
            crPalCol[12][0]=RGB(128,0,255);
            crPalCol[13][0]=RGB(128,64,0);
            crPalCol[0][1]=RGB(255,255,255);
            crPalCol[1][1]=RGB(192,192,192);
            crPalCol[2][1]=RGB(255,0,0);
            crPalCol[3][1]=RGB(255,255,0);
            crPalCol[4][1]=RGB(0,255,0);
            crPalCol[5][1]=RGB(0,255,255);
            crPalCol[6][1]=RGB(0,0,255);
            crPalCol[7][1]=RGB(255,0,255);
            crPalCol[8][1]=RGB(255,255,128);
            crPalCol[9][1]=RGB(0,255,128);
            crPalCol[10][1]=RGB(128,255,255);
            crPalCol[11][1]=RGB(128,128,255);
            crPalCol[12][1]=RGB(255,0,128);
            crPalCol[13][1]=RGB(255,128,64);
            crCurUp = crPalCol[0][0];
            crCurDown = crPalCol[0][1];
            IsUpColor = TRUE; // ������� ����� ����

            /////////////////////// �������� �������� GDI //////////////////////
            hPen			= CreatePen(PS_SOLID,0,0);
            hPenL			= CreatePen(PS_SOLID,0,RGB(236,233,216));	// ������� ������� ����������� ����
            hPenD			= CreatePen(PS_SOLID,0,RGB(172,168,153));	// ������� ������ ����������� ����
            hSelectPen		= CreatePen(PS_SOLID,0,crCurUp);			// ������� ����
            hBrush			= CreateSolidBrush(0);
            hSelectBrush	= CreateSolidBrush(crCurDown);				// ������� �����

            ////////////////////////////////////////////////////////////////////
            ShowWindow(hwnd_list,SW_NORMAL);
            ShowWindow(hwnd_obj_propert,SW_NORMAL);
            ShowWindow(hwndPalette,SW_NORMAL);
            ShowWindow(hwndLayer,SW_NORMAL);
            ShowWindow(hwndHolst,SW_NORMAL);
        }
        case WM_PAINT:
        {
            BeginPaint(hwnd, &ps);
            EndPaint(hwnd, &ps);
            UpdateWindow(hwndMain);
            break;
        }
        case WM_SIZE:
        {
            GetWindowRect(hwndMain, &rect);
            if ((rect.bottom-rect.top)<650)
            {
                rect.bottom=rect.top+650;
            }
            if ((rect.right-rect.left)<800+270)
            {
                rect.right=rect.left+800+270;
            }
            MoveWindow(hwndMain,rect.left,rect.top,rect.right-rect.left,rect.bottom-rect.top,TRUE);
            MoveWindow(hwndList,0,325,260,rect.bottom-rect.top-325-50,TRUE);
            break;
        }
        case WM_COMMAND:
        {
            if (HIWORD(wParam)==0)
                switch(LOWORD(wParam))
                {
                    case MI_EXIT:
                    {
                        PostQuitMessage (0);
                        break;
                    }
                    case MI_ABOUT:
                    {
                        hwndAbout = CreateWindowEx(
                                             0,
                                             "about",
                                             "� ���������",
                                             WS_OVERLAPPED|WS_BORDER|WS_SYSMENU,
                                             440,
                                             280,
                                             360,
                                             200,
                                             hwnd,
                                             NULL,
                                             hInst,
                                             NULL);
                        ShowWindow(hwndAbout,SW_NORMAL);
                        UpdateWindow(hwndAbout);
                        SetFocus(hwndAbout);
                        EnableWindow(hwndMain,0);
                        break;
                    }
                    case MI_FONT:
                    {
                        if (id!=-1 && shapes[id]->i==6)
                        {
                            if (ChooseFont(&(shapes[id]->cf)))
                            {
                                crCurUp=shapes[id]->cf.rgbColors;
                                RisPal();
                                InvalidateRect(hwndPalette, NULL, TRUE);
                                UpdateWindow(hwndPalette);
                                InvalidateRect(hwndHolst, NULL, TRUE);
                                UpdateWindow(hwndHolst);
                            }
                        }
                        break;
                    }
                    case MI_SAVE:
                    {
                        if (TitleFile2[0]!='\0')
                            save_as(File2);
                        else
                        {
                            memset(File2, '\0', sizeof(File2));
                            memset(TitleFile2, '\0', sizeof(TitleFile2));
                            if (GetSaveFileName(&svld1))
                            {
                                if(File2[strlen(File2)-4]!='.' || File2[strlen(File2)-2]!='V' ||File2[strlen(File2)-1]!='G')
                                {
                                    File2[strlen(File2)]='.';
                                    File2[strlen(File2)]='V';
                                    File2[strlen(File2)]='G';
                                }
                                save_as(File2);
                                memset(WinText, '\0', sizeof(WinText));
                                strcpy(WinText,TitleFile2);
                                WinText[strlen(WinText)]='.';
                                WinText[strlen(WinText)]='V';
                                WinText[strlen(WinText)]='G';
                                WinText[strlen(WinText)]=' ';
                                WinText[strlen(WinText)]='-';
                                WinText[strlen(WinText)]=' ';
                                strcat(WinText,szAppName);
                                SetWindowText(hwndMain,WinText);
                            }
                        }
                        break;
                    }
                    case MI_SAVE_AS:
                    {
                        memset(File2, '\0', sizeof(File2));
                        memset(TitleFile2, '\0', sizeof(TitleFile2));
                        if (GetSaveFileName(&svld1))
                        {
                            if(File2[strlen(File2)-4]!='.' || File2[strlen(File2)-2]!='V' ||File2[strlen(File2)-1]!='G')
                            {
                                File2[strlen(File2)]='.';
                                File2[strlen(File2)]='V';
                                File2[strlen(File2)]='G';
                            }
                            save_as(File2);
                            memset(WinText, '\0', sizeof(WinText));
                            strcpy(WinText,TitleFile2);
                            WinText[strlen(WinText)]='.';
                            WinText[strlen(WinText)]='V';
                            WinText[strlen(WinText)]='G';
                            WinText[strlen(WinText)]=' ';
                            WinText[strlen(WinText)]='-';
                            WinText[strlen(WinText)]=' ';
                            strcat(WinText,szAppName);
                            SetWindowText(hwndMain,WinText);
                        }
                        break;
                    }
                    case MI_OPEN:
                    {
                        memset(File2, '\0', sizeof(File2));
                        memset(TitleFile2, '\0', sizeof(File2));
                        if (GetOpenFileName(&svld1))
                        {
                            shapes.clear();
                            status=1;
                            numBord=0;
                            bTracking=0;
                            ed_text=0;
                            selected=0;
                            id=-1;
                            InvalidateRect(hwnd_obj_propert, NULL, TRUE);
                            UpdateWindow(hwnd_obj_propert);
                            open_file(File2);
                            memset(WinText, '\0', sizeof(WinText));
                            strcpy(WinText,TitleFile2);
                            WinText[strlen(WinText)]=' ';
                            WinText[strlen(WinText)]='-';
                            WinText[strlen(WinText)]=' ';
                            strcat(WinText,szAppName);
                            SetWindowText(hwndMain,WinText);
                            id=-1;
                            InvalidateRect(hwndHolst, NULL, TRUE);
                            UpdateWindow(hwndHolst);
                            fill_list();
                        }
                        break;
                    }
                    case MI_EXPORT_SVG:
                    {
                        memset(File, '\0', sizeof(File));
                        if (GetSaveFileName(&svld))
                        {
                            if(File[strlen(File)-4]!='.' || File[strlen(File)-3]!='S' || File[strlen(File)-2]!='V' ||File[strlen(File)-1]!='G')
                            {
                                File[strlen(File)]='.';
                                File[strlen(File)]='S';
                                File[strlen(File)]='V';
                                File[strlen(File)]='G';
                            }
                            save_svg(File);
                        }
                        break;
                    }
                    case MI_ROLATION_R:
                    {
                        if (id!=-1)
                        {
                            shapes[id]->turn_obj(1.5708);
                            InvalidateRect(hwndHolst, NULL, TRUE);
                            UpdateWindow(hwndHolst);
                        }
                        break;
                    }
                    case MI_ROLATION_L:
                    {
                        if (id!=-1)
                        {
                            shapes[id]->turn_obj(-1.5708);
                            InvalidateRect(hwndHolst, NULL, TRUE);
                            UpdateWindow(hwndHolst);
                        }
                        break;
                    }
                    case MI_LAYER_DEL:
                    {
                        if (id!=-1)
                        {
                            shapes.erase(shapes.begin() + id);
                            id=-1;
                            InvalidateRect(hwndHolst, NULL, TRUE);
                            UpdateWindow(hwndHolst);
                            fill_list();
                        }
                        break;
                    }
                    case MI_LAYER_UP1:
                    {
                        if (id!=-1 && id!=shapes.size()-1)
                        {
                            swap(shapes[id+1],shapes[id]);
                            id+=1;
                            InvalidateRect(hwndHolst, NULL, TRUE);
                            UpdateWindow(hwndHolst);
                            fill_list();
                        }
                        break;
                    }
                    case MI_LAYER_DOWN1:
                    {
                        if (id!=-1 && id!=0)
                        {
                            swap(shapes[id],shapes[id-1]);
                            id-=1;
                            InvalidateRect(hwndHolst, NULL, TRUE);
                            UpdateWindow(hwndHolst);
                            fill_list();
                        }
                        break;
                    }
                    case MI_LAYER_UP:
                    {
                        if (id!=-1 && id!=shapes.size()-1)
                        {
                            for(int i=id;i<shapes.size()-1;i++)
                                swap(shapes[i],shapes[i+1]);
                            id=shapes.size()-1;
                            InvalidateRect(hwndHolst, NULL, TRUE);
                            UpdateWindow(hwndHolst);
                            fill_list();
                        }
                        break;
                    }
                    case MI_LAYER_DOWN:
                    {
                        if (id!=-1 && id!=0)
                        {
                            for(int i=id;i>0;i--)
                                swap(shapes[i],shapes[i-1]);
                            id=0;
                            InvalidateRect(hwndHolst, NULL, TRUE);
                            UpdateWindow(hwndHolst);
                            fill_list();
                        }
                        break;
                    }
                }
            break;
        }
        case WM_DESTROY:
        {
            DeleteObject(hPen);
            DeleteObject(hPenL);		    // ������ ������� ������ ����
            DeleteObject(hPenD);		    // ������ ������ ������ ����
            DeleteObject(hSelectPen);		// ������ ����
            DeleteObject(hBrush);
            DeleteObject(hSelectBrush);		// ������ �����
            DeleteDC(hdcMemPalette);
            DeleteDC(hdcMemHolstBack);
            DeleteDC(hdcMemHolstDyn);
            shapes.clear();
            PostQuitMessage(0);
            PostQuitMessage (0);
            break;
        }
        default:
            return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}

LRESULT CALLBACK AboutProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HWND hwnd_close,my_text;
    HDC hdc;
    switch(message)
    {
        case WM_CREATE:
        {
            SetBkMode(hdc,TRANSPARENT);
            TextOut(hdc,110,10,"� ���������",11);

            my_text = CreateWindowEx(
                      0,
                      "STATIC",
                      "��������� ����������� ��������\n\n ��������� ����������:\n-������������ �������\n  Windows XP � ������\n-����������\n  ����������� ���������� ������ 1024x768\n\n�����������: ������� �.�.",
                      WS_CHILD|WS_VISIBLE|SS_LEFT,
                      20,
                      20,
                      320,
                      200,
                      hwnd,
                      (HMENU)NULL,
                      hInst,
                      NULL);
        }
        case WM_PAINT:
        {
            BeginPaint(hwnd, &ps);
            EndPaint(hwnd, &ps);
            break;
        }
        case WM_COMMAND:
        {
            if (HIWORD(wParam)==0 && LOWORD(wParam)==BT_CLOSE)
                SendMessage(hwndAbout,WM_DESTROY,0,0);
            break;
        }
        case WM_DESTROY:
        {
            EnableWindow(hwndMain,1);
            DeleteObject(hwndAbout);
            SetFocus(hwndMain);
            DestroyWindow(hwnd);
        }
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK ListObjectProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    switch(message)
    {
        case WM_CREATE:
        {
            hwB0=CreateWindowEx(
                                0,
                                "B0",
                                "1",
                                WS_CHILD|WS_BORDER|WS_VISIBLE ,
                                26*0+2*1,2,26,26,
                                hwnd,
                                NULL,
                                hInst,
                                NULL);
            hwB1=CreateWindowEx(
                                0,
                                "B1",
                                "",
                                WS_CHILD|WS_BORDER|WS_VISIBLE ,
                                26*1+2*2,2,26,26,
                                hwnd,
                                NULL,
                                hInst,
                                NULL);
            hwB3=CreateWindowEx(
                                0,
                                "B3",
                                "",
                                WS_CHILD|WS_BORDER|WS_VISIBLE ,
                                26*2+2*3,2,26,26,
                                hwnd,
                                NULL,
                                hInst,
                                NULL);
            hwB4=CreateWindowEx(
                                0,
                                "B4",
                                "",
                                WS_CHILD|WS_BORDER|WS_VISIBLE ,
                                26*3+2*4,2,26,26,
                                hwnd,
                                NULL,
                                hInst,
                                NULL);
            hwB5=CreateWindowEx(
                                0,
                                "B5",
                                "",
                                WS_CHILD|WS_BORDER|WS_VISIBLE ,
                                26*4+2*5,2,26,26,
                                hwnd,
                                NULL,
                                hInst,
                                NULL);
            hwB6=CreateWindowEx(
                                0,
                                "B6",
                                "",
                                WS_CHILD|WS_BORDER|WS_VISIBLE ,
                                26*5+2*6,2,26,26,
                                hwnd,
                                NULL,
                                hInst,
                                NULL);
            hwB8=CreateWindowEx(
                                0,
                                "B8",
                                "",
                                WS_CHILD|WS_BORDER|WS_VISIBLE ,
                                26*6+2*7,2,26,26,
                                hwnd,
                                NULL,
                                hInst,
                                NULL);
            hwB9=CreateWindowEx(
                                0,
                                "B9",
                                "",
                                WS_CHILD|WS_BORDER|WS_VISIBLE ,
                                26*8+2*9,2,26,26,
                                hwnd,
                                NULL,
                                hInst,
                                NULL);
            ShowWindow(hwB0,SW_NORMAL);
            ShowWindow(hwB1,SW_NORMAL);
            ShowWindow(hwB3,SW_NORMAL);
            ShowWindow(hwB4,SW_NORMAL);
            ShowWindow(hwB5,SW_NORMAL);
            ShowWindow(hwB6,SW_NORMAL);
            ShowWindow(hwB8,SW_NORMAL);
            ShowWindow(hwB9,SW_NORMAL);
        }
        case WM_PAINT:
        {
            BeginPaint(hwnd, &ps);
            EndPaint(hwnd, &ps);
            break;
        }
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK ObjectPropProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    int x=status;
    INFO inf;
    char buf[20]="\n";
    switch(message)
    {
        case WM_CREATE:
        {
            hwndEdit_R1     = CreateWindow("edit", "", ES_READONLY|WS_CHILD|ES_LEFT|ES_NUMBER|WS_VISIBLE, 70, 80,  40, 17, hwnd, NULL, NULL, NULL);
            hwndEdit_R2     = CreateWindow("edit", "", ES_READONLY|WS_CHILD|ES_LEFT|ES_NUMBER|WS_VISIBLE, 70, 100, 40, 17, hwnd, NULL, NULL, NULL);
            hwndEdit_X      = CreateWindow("edit", "", ES_READONLY|WS_CHILD|ES_LEFT|ES_NUMBER|WS_VISIBLE, 70, 120, 40, 17, hwnd, NULL, NULL, NULL);
            hwndEdit_Y      = CreateWindow("edit", "", ES_READONLY|WS_CHILD|ES_LEFT|ES_NUMBER|WS_VISIBLE, 70, 140, 40, 17, hwnd, NULL, NULL, NULL);
            hwndEdit_width  = CreateWindow("edit", "", ES_READONLY|WS_CHILD|ES_LEFT|ES_NUMBER|WS_VISIBLE, 70, 160, 40, 17, hwnd, NULL, NULL, NULL);
            hwndEdit_height = CreateWindow("edit", "", ES_READONLY|WS_CHILD|ES_LEFT|ES_NUMBER|WS_VISIBLE, 70, 180, 40, 17, hwnd, NULL, NULL, NULL);
            hwndEdit_num    = CreateWindow("edit", "7", WS_CHILD|ES_LEFT|ES_NUMBER|WS_VISIBLE, 70, 180, 40, 17, hwnd, NULL, NULL, NULL);
            hwndEdit_text   = CreateWindow("edit", "", WS_CHILD|WS_BORDER|ES_LEFT|WS_VISIBLE|ES_MULTILINE|WS_VSCROLL, 70, 180, 40, 17, hwnd, NULL, NULL, NULL);
            hwndBut_enter=CreateWindowEx(
                                0,
                                "B_enter",
                                "",
                                WS_CHILD|WS_BORDER|WS_VISIBLE ,
                                190,2,26,117,
                                hwnd,
                                NULL,
                                hInst,
                                NULL);
        }
        case WM_PAINT:
        {
            if (id!=-1)
            {
                x=shapes[id]->i+3;
                shapes[id]->get_info(&inf);
            }
            switch(x)
            {
                case 1:
                case 10:
                {
                    MoveWindow(hwndEdit_R1,300, 80, 40, 17,TRUE);
                    MoveWindow(hwndEdit_R2,300, 80, 40, 17,TRUE);
                    MoveWindow(hwndEdit_X,300, 80, 40, 17,TRUE);
                    MoveWindow(hwndEdit_Y,300, 80, 40, 17,TRUE);
                    MoveWindow(hwndEdit_width,300, 80, 40, 17,TRUE);
                    MoveWindow(hwndEdit_height,300, 80, 40, 17,TRUE);
                    MoveWindow(hwndEdit_num,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_text,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndBut_enter,300, 85, 40, 17,TRUE);

                    DestroyWindow(tx1);
                    DestroyWindow(tx2);
                    DestroyWindow(tx3);
                    DestroyWindow(tx4);
                    DestroyWindow(tx5);
                    DestroyWindow(tx6);
                    DestroyWindow(tx7);

                    SetWindowTextA(hwndEdit_X,buf);
                    SetWindowTextA(hwndEdit_Y,buf);
                    SetWindowTextA(hwndEdit_R1,buf);
                    SetWindowTextA(hwndEdit_R2,buf);
                    SetWindowTextA(hwndEdit_width,buf);
                    SetWindowTextA(hwndEdit_height,buf);
                    SetWindowTextA(hwndEdit_num,buf);
                    break;
                }
                case 4:
                {
                    DestroyWindow(tx1);
                    DestroyWindow(tx2);
                    DestroyWindow(tx3);
                    DestroyWindow(tx4);
                    DestroyWindow(tx5);
                    DestroyWindow(tx6);
                    DestroyWindow(tx7);
                    tx1=CreateWindow("static", "���������� ������:", WS_CHILD|WS_VISIBLE, 10, 10, 180, 18, hwnd, NULL, NULL, NULL);
                    tx2=CreateWindow("static", "X = ", WS_CHILD|WS_VISIBLE, 10, 35, 30, 20, hwnd, NULL, NULL, NULL);
                    tx3=CreateWindow("static", "Y = ", WS_CHILD|WS_VISIBLE, 10, 60, 30, 20, hwnd, NULL, NULL, NULL);
                    tx4=CreateWindow("static", "������:", WS_CHILD|WS_VISIBLE, 10, 85, 60, 20, hwnd, NULL, NULL, NULL);
                    tx5=CreateWindow("static", "������:", WS_CHILD|WS_VISIBLE, 10, 110, 60, 20, hwnd, NULL, NULL, NULL);
                    MoveWindow(hwndEdit_X,41, 35, 40, 17,TRUE);
                    MoveWindow(hwndEdit_Y,41, 60, 40, 17,TRUE);
                    MoveWindow(hwndEdit_width,71, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_height,71, 110, 40, 17,TRUE);
                    if (id!=-1)
                    {
                        sprintf(buf,"%d\n",inf.X);
                        SetWindowTextA(hwndEdit_X,buf);

                        sprintf(buf,"%d\n",inf.Y);
                        SetWindowTextA(hwndEdit_Y,buf);

                        sprintf(buf,"%d\n",inf.W);
                        SetWindowTextA(hwndEdit_width,buf);

                        sprintf(buf,"%d\n",inf.H);
                        SetWindowTextA(hwndEdit_height,buf);
                    }
                    else
                    {
                        SetWindowTextA(hwndEdit_X,buf);
                        SetWindowTextA(hwndEdit_Y,buf);
                        SetWindowTextA(hwndEdit_width,buf);
                        SetWindowTextA(hwndEdit_height,buf);
                    }
                    MoveWindow(hwndEdit_R1,300, 80, 40, 17,TRUE);
                    MoveWindow(hwndEdit_R2,300, 80, 40, 17,TRUE);
                    MoveWindow(hwndEdit_num,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_text,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndBut_enter,300, 85, 40, 17,TRUE);
                    break;
                }
                case 5:
                {
                    DestroyWindow(tx1);
                    DestroyWindow(tx2);
                    DestroyWindow(tx3);
                    DestroyWindow(tx4);
                    DestroyWindow(tx5);
                    DestroyWindow(tx6);
                    DestroyWindow(tx7);
                    tx1=CreateWindow("static", "���������� ������:", WS_CHILD|WS_VISIBLE, 10, 10, 180, 18, hwnd, NULL, NULL, NULL);
                    tx2=CreateWindow("static", "X = ", WS_CHILD|WS_VISIBLE, 10, 35, 30, 20, hwnd, NULL, NULL, NULL);
                    tx3=CreateWindow("static", "Y = ", WS_CHILD|WS_VISIBLE, 10, 60, 30, 20, hwnd, NULL, NULL, NULL);
                    tx4=CreateWindow("static", "������ �� ���������:", WS_CHILD|WS_VISIBLE, 10, 85, 150, 20, hwnd, NULL, NULL, NULL);
                    tx5=CreateWindow("static", "������ �� �����������:", WS_CHILD|WS_VISIBLE, 10, 110, 170, 20, hwnd, NULL, NULL, NULL);
                    MoveWindow(hwndEdit_X,41, 35, 40, 17,TRUE);
                    MoveWindow(hwndEdit_Y,41, 60, 40, 17,TRUE);
                    MoveWindow(hwndEdit_R1,181, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_R2,181, 110, 40, 17,TRUE);
                    if (id!=-1)
                    {
                        sprintf(buf,"%d\n",inf.X);
                        SetWindowTextA(hwndEdit_X,buf);

                        sprintf(buf,"%d\n",inf.Y);
                        SetWindowTextA(hwndEdit_Y,buf);

                        sprintf(buf,"%d\n",inf.R1);
                        SetWindowTextA(hwndEdit_R1,buf);

                        sprintf(buf,"%d\n",inf.R2);
                        SetWindowTextA(hwndEdit_R2,buf);
                    }
                    else
                    {
                        SetWindowTextA(hwndEdit_X,buf);
                        SetWindowTextA(hwndEdit_Y,buf);
                        SetWindowTextA(hwndEdit_R1,buf);
                        SetWindowTextA(hwndEdit_R2,buf);
                    }
                    MoveWindow(hwndEdit_width,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_height,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_num,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_text,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndBut_enter,300, 85, 40, 17,TRUE);
                    break;
                }
                case 6:
                {
                    DestroyWindow(tx1);
                    DestroyWindow(tx2);
                    DestroyWindow(tx3);
                    DestroyWindow(tx4);
                    DestroyWindow(tx5);
                    DestroyWindow(tx6);
                    DestroyWindow(tx7);
                    tx1=CreateWindow("static", "���������� ������:", WS_CHILD|WS_VISIBLE, 10, 10, 180, 18, hwnd, NULL, NULL, NULL);
                    tx2=CreateWindow("static", "X = ", WS_CHILD|WS_VISIBLE, 10, 35, 30, 20, hwnd, NULL, NULL, NULL);
                    tx3=CreateWindow("static", "Y = ", WS_CHILD|WS_VISIBLE, 10, 60, 30, 20, hwnd, NULL, NULL, NULL);
                    tx4=CreateWindow("static", "������:", WS_CHILD|WS_VISIBLE, 10, 85, 60, 20, hwnd, NULL, NULL, NULL);
                    tx5=CreateWindow("static", "���������� ����� N:", WS_CHILD|WS_VISIBLE, 10, 110, 140, 18, hwnd, NULL, NULL, NULL);
                    tx6=CreateWindow("static", "���������: N ����������� [3;12]", WS_CHILD|WS_VISIBLE, 10, 135, 300, 18, hwnd, NULL, NULL, NULL);
                    MoveWindow(hwndEdit_X,41, 35, 40, 17,TRUE);
                    MoveWindow(hwndEdit_Y,41, 60, 40, 17,TRUE);
                    MoveWindow(hwndEdit_R1,71, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_num,160, 110, 40, 17,TRUE);
                    if (id!=-1)
                    {
                        sprintf(buf,"%d\n",inf.X);
                        SetWindowTextA(hwndEdit_X,buf);

                        sprintf(buf,"%d\n",inf.Y);
                        SetWindowTextA(hwndEdit_Y,buf);

                        sprintf(buf,"%d\n",inf.R1);
                        SetWindowTextA(hwndEdit_R1,buf);

                        sprintf(buf,"%d\n",inf.N);
                        SetWindowTextA(hwndEdit_num,buf);
                    }
                    else
                    {
                        SetWindowTextA(hwndEdit_X,buf);
                        SetWindowTextA(hwndEdit_Y,buf);
                        SetWindowTextA(hwndEdit_R1,buf);
                        sprintf(buf,"%d\n",7);
                        SetWindowTextA(hwndEdit_num,buf);
                    }
                    MoveWindow(hwndEdit_R2,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_width,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_height,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_text,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndBut_enter,300, 85, 40, 17,TRUE);
                    break;
                }
                case 7:
                {
                    DestroyWindow(tx1);
                    DestroyWindow(tx2);
                    DestroyWindow(tx3);
                    DestroyWindow(tx4);
                    DestroyWindow(tx5);
                    DestroyWindow(tx6);
                    DestroyWindow(tx7);
                    tx1=CreateWindow("static", "���������� ������:", WS_CHILD|WS_VISIBLE, 10, 10, 180, 18, hwnd, NULL, NULL, NULL);
                    tx2=CreateWindow("static", "X = ", WS_CHILD|WS_VISIBLE, 10, 35, 30, 20, hwnd, NULL, NULL, NULL);
                    tx3=CreateWindow("static", "Y = ", WS_CHILD|WS_VISIBLE, 10, 60, 30, 20, hwnd, NULL, NULL, NULL);
                    MoveWindow(hwndEdit_X,41, 35, 40, 17,TRUE);
                    MoveWindow(hwndEdit_Y,41, 60, 40, 17,TRUE);
                    if (id!=-1)
                    {
                        sprintf(buf,"%d\n",inf.X);
                        SetWindowTextA(hwndEdit_X,buf);

                        sprintf(buf,"%d\n",inf.Y);
                        SetWindowTextA(hwndEdit_Y,buf);
                    }
                    else
                    {
                        SetWindowTextA(hwndEdit_X,buf);
                        SetWindowTextA(hwndEdit_Y,buf);
                    }
                    MoveWindow(hwndEdit_R1,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_R2,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_width,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_height,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_num,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndEdit_text,300, 85, 40, 17,TRUE);
                    MoveWindow(hwndBut_enter,300, 85, 40, 17,TRUE);
                    break;
                }
                case 9:
                {
                    DestroyWindow(tx1);
                    DestroyWindow(tx2);
                    DestroyWindow(tx3);
                    DestroyWindow(tx4);
                    DestroyWindow(tx5);
                    DestroyWindow(tx6);
                    DestroyWindow(tx7);

                    tx1=CreateWindow("static", "���������� ������:", WS_CHILD|WS_VISIBLE, 10, 10, 180, 18, hwnd, NULL, NULL, NULL);
                    tx2=CreateWindow("static", "X = ", WS_CHILD|WS_VISIBLE, 10, 35, 30, 20, hwnd, NULL, NULL, NULL);
                    tx3=CreateWindow("static", "Y = ", WS_CHILD|WS_VISIBLE, 10, 60, 30, 20, hwnd, NULL, NULL, NULL);
                    tx4=CreateWindow("static", "�����:", WS_CHILD|WS_VISIBLE, 10, 85, 60, 20, hwnd, NULL, NULL, NULL);

                    MoveWindow(hwndEdit_X,41, 35, 40, 17,TRUE);
                    MoveWindow(hwndEdit_Y,41, 60, 40, 17,TRUE);
                    MoveWindow(hwndEdit_text,10,110, 230, 70,TRUE);
                    if (id!=-1 && shapes[id]->i==6)
                        MoveWindow(hwndBut_enter,30,130+55, 117, 26,TRUE);
                    else
                        MoveWindow(hwndBut_enter,300, 85, 40, 17,TRUE);
                    if (id!=-1)
                    {
                        sprintf(buf,"%d\n",inf.X);
                        SetWindowTextA(hwndEdit_X,buf);

                        sprintf(buf,"%d\n",inf.Y);
                        SetWindowTextA(hwndEdit_Y,buf);

                        char buf2[inf.N];
                        for(int i=0;i<inf.N;i++)
                            buf2[i]=inf.st[i];
                        buf2[inf.N]='\0';
                        SetWindowTextA(hwndEdit_text,buf2);
                    }
                    else
                    {
                        SetWindowTextA(hwndEdit_X,buf);
                        SetWindowTextA(hwndEdit_Y,buf);
                        buf[0]='\0';
                        SetWindowTextA(hwndEdit_text,buf);
                    }

                    MoveWindow(hwndEdit_R1,300, 80, 40, 17,TRUE);
                    MoveWindow(hwndEdit_R2,300, 80, 40, 17,TRUE);
                    MoveWindow(hwndEdit_width,300, 80, 40, 17,TRUE);
                    MoveWindow(hwndEdit_height,300, 80, 40, 17,TRUE);
                    MoveWindow(hwndEdit_num,300, 85, 40, 17,TRUE);
                    break;
                }
            }
            BeginPaint(hwnd, &ps);
            EndPaint(hwnd, &ps);
            UpdateWindow(hwnd);
            break;
        }
        case WM_COMMAND:
        {
            if (HIWORD(wParam)==0 && LOWORD(wParam)==BT_ENTER)
            {
                if (id!=-1 && shapes[id]->i==6)
                {
                    int kol=SendMessage(hwndEdit_text,WM_GETTEXTLENGTH,0,0);
                    cout<<kol<<endl;
                    string st;
                    char buf[kol+1];
                    GetWindowText(hwndEdit_text,buf,kol+1);
                    buf[kol]='\0';
                    shapes[id]->add_char(buf,kol+1);
                    InvalidateRect(hwndHolst, NULL, TRUE);
                    UpdateWindow(hwndHolst);
                }
            }
            break;
        }
        case WM_DESTROY:
            break;
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK WndPalette (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    int x,y;
	PAINTSTRUCT	ps;
	switch (message)
	{
		case WM_LBUTTONDOWN:
		{
		    x = LOWORD(lParam);
            y = HIWORD(lParam);
            if ((x>30) && (x<260))
            {
                IsUpColor = TRUE;
                tekI = (x - 31)/16;
                tekJ = (y)/16;
                crCurUp = crPalCol[tekI][tekJ];
                RisPal();
            }
            UpdateWindow(hwndPalette);
            break;
		}
        case WM_RBUTTONDOWN:
		{
		    x = LOWORD(lParam);
            y = HIWORD(lParam);
            if (x > 30)
            {
                IsUpColor = FALSE;
                tekI = (x - 31)/16;
                tekJ = y/16;
                crCurDown = crPalCol[tekI][tekJ];
                RisPal();
            }
            UpdateWindow(hwndPalette);
            break;
		}
		case WM_PAINT:
        {
            hdcPalette= BeginPaint(hwnd,&ps);
            BitBlt(hdcPalette, 0, 0, 254, 31,	hdcMemPalette, 0, 0, SRCCOPY);
            EndPaint(hwnd,&ps);
            break;
        }
	}
	return DefWindowProc(hwnd, message, wParam, lParam);
}

//////////////////////////////// ���������� ������� ///////////////////////////////////////////////
int RisPal(void)
{
	DeleteObject(hBrush);									// ���������� �����
	hBrush = CreateSolidBrush(crCurDown);					// ������� ����� �� ������ ������� �����
	SelectObject(hdcMemPalette,hBrush);						// ������� �����
	SelectObject(hdcMemPalette,GetStockObject(NULL_PEN));	// ������� ������ ����
	Rectangle(hdcMemPalette,13,14,25,26);					// ���������� ������ �������

	SelectObject(hdcMemPalette,hPenD);						// ������� ����
	MoveToEx(hdcMemPalette,18,6,NULL);						// ��������� ����
	LineTo(hdcMemPalette,18,19);							// ������ �����
	LineTo(hdcMemPalette,4,19);								// ������ �����
	SelectObject(hdcMemPalette,GetStockObject(WHITE_PEN));	// ������� ����� ����
	LineTo(hdcMemPalette,4,5);								// ����� �����
	LineTo(hdcMemPalette,19,5);								// ������� �����

	DeleteObject(hBrush);									// ���������� �����
	hBrush	= CreateSolidBrush(crCurUp);					// ������� ����� �� ������ �������� �����
	SelectObject(hdcMemPalette,hPenL);						// ������� ������� ����
	SelectObject(hdcMemPalette,hBrush);						// ������� �����
	Rectangle(hdcMemPalette,5,6,18,19);						// ���������� ������� �������

	for (int i=0;i<14;i++)
	for (int j=0;j<2;j++)
	{
		DeleteObject(hBrush);									// ������� �����
		hBrush		= CreateSolidBrush(crPalCol[i][j]);			// ������� ����� �����
		SelectObject(hdcMemPalette,GetStockObject(NULL_PEN));	// ������� ���������� ����
		SelectObject(hdcMemPalette,hBrush);						// ������� ������� �����
		Rectangle(hdcMemPalette,i * 16 +33,j * 16 +2,i * 16 + 46,j * 16+15); // ���������� ������
	}
	InvalidateRect(hwndPalette,NULL,TRUE);						// ����������� ��� ��������
	return 0;
}

LRESULT CALLBACK WndHolst(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    static HDC hDC; // �������� ���������� ��� ��������� �����
	static int xDown,yDown,xMove,yMove;
	static int max_x=-1, min_x=INFINITE, max_y=-1, min_y=INFINITE;
	static int num_top=-1;
	char c[10];
	char buf[20];
	PAINTSTRUCT	psHolst;
	HBITMAP	hBitmapHolstBack,hBitmapHolstDyn;	// ��������
	RECT re;
	INFO inf;
	switch (message)
	{
        case WM_CREATE:
        {
            hDC = GetDC(hWnd);
            GetClientRect(hWnd,&re);
            //��������� �������� �����
            SetMapMode(hdcMemHolstBack,MM_HIMETRIC);
            SetMapMode(hdcMemHolstDyn,MM_HIMETRIC);
            SetMapMode(hdcHolst,MM_HIMETRIC);

            hdcMemHolstBack = CreateCompatibleDC(GetDC(hwndHolst));					// �������� ������ "�������"
            hdcMemHolstDyn = CreateCompatibleDC(GetDC(hwndHolst));					// �������� ������ "��������������"
            hBitmapHolstBack = CreateCompatibleBitmap(GetDC(hwndHolst),800,600);	// ������� ������ ��������
            hBitmapHolstDyn = CreateCompatibleBitmap(GetDC(hwndHolst),800,600);		// ������� ������ ��������
            SelectObject(hdcMemHolstBack,hBitmapHolstBack);							// ������� � ������� ��������
            SelectObject(hdcMemHolstDyn,hBitmapHolstDyn);							// ������� � �������������� ��������

            SelectObject(hdcMemHolstBack,GetStockObject(WHITE_BRUSH));
            SelectObject(hdcMemHolstBack,GetStockObject(BLACK_PEN));
            Rectangle(hdcMemHolstBack,0,0,800,600);									// ������� ������� �� ��� ������� �����������
        }
        case WM_ERASEBKGND:
            return FALSE;
        case WM_PAINT:
        {
            hdcHolst = BeginPaint(hWnd,&psHolst);
            RepaintBack();
            BitBlt(hdcHolst, 0, 0, 800, 600,	hdcMemHolstBack, 0, 0, SRCCOPY);
            EndPaint(hWnd,&psHolst);
            break;
        }
        case WM_LBUTTONDOWN:
        {
            xMove = xDown = LOWORD(lParam);
            yMove = yDown = HIWORD(lParam);
            switch(status)
            {
                case 1:
                {
                    if (id!=-1)
                    {
                        numBord=shapes[id]->accessory_bord(xDown,yDown);
                        if (numBord)
                        {
                            selected=true;
                            break;
                        }
                    }
                    for (int i = shapes.size()-1; i>-1; i--) // ������� �����
                    {
                        if (shapes[i]->accessory(xDown,yDown))
                        {
                            selected=true;
                            id=i;
                            SendMessage(hwndList,LB_SETCURSEL,id,0);
                            SendMessage(hwnd_obj_propert,WM_PAINT,0,0);
                            InvalidateRect(hwnd_obj_propert,NULL,TRUE);
                            UpdateWindow(hwnd_obj_propert);
                            break;
                        }
                        else
                        {
                            selected=false;
                            id=-1;
                            fill_list();
                            InvalidateRect(hwnd_obj_propert,NULL,TRUE);
                            UpdateWindow(hwnd_obj_propert);
                        }
                    }
                    if (id==-1 && shapes.size())
                    {
                        InvalidateRect(hwndHolst, NULL, TRUE);
                        UpdateWindow(hwndHolst);
                    }
                    break;
                }
                case 4:
                {
                    id=-1;
                    bTracking = TRUE;
                    break;
                }
                case 5:
                {
                    id=-1;
                    bTracking = TRUE;
                    break;
                }
                case 6:
                {
                    SendMessage(hwndEdit_num, WM_GETTEXT, sizeof(c), (LPARAM)c);
                    num_top=atoi(c);
                    if (num_top<3 || num_top>12)
                        break;
                    id=-1;
                    bTracking = TRUE;
                    el_polygon=new polygon(&crCurUp,&crCurDown,num_top,xDown,yDown);
                    break;
                }
                case 7:
                {
                    id=-1;
                    bTracking = TRUE;
                    el_FreeLine=new FreeLine(&crCurUp);
                    max_x=min_x=xDown;
                    max_y=min_y=yDown;

                    MoveToEx(hDC, xDown, yDown, NULL);
                    el_FreeLine->points.push_back(Point(xDown, yDown));
                    break;
                }
                case 10:
                {
                    for (i = shapes.size()-1; i>-1; i--)
                    {
                        if (shapes[i]->accessory(xDown,yDown))
                        {
                            id=i;
                            break;
                        }
                        else
                            id=-1;
                    }
                    if (id!=-1)
                    {
                        shapes[id]->fill_obj(&crCurUp,&crCurDown);
                        InvalidateRect(hwndHolst, NULL, TRUE);
                        UpdateWindow(hwndHolst);
                        InvalidateRect(hwnd_obj_propert,NULL,TRUE);
                        UpdateWindow(hwnd_obj_propert);
                        SendMessage(hwndList,LB_SETCURSEL,id,0);
                        if (shapes[id]->i==4)
                        {
                            shapes[id]->get_info(&inf);
                            sprintf(buf,"%d\n",inf.X);
                            SetWindowTextA(hwndEdit_X,buf);

                            sprintf(buf,"%d\n",inf.Y);
                            SetWindowTextA(hwndEdit_Y,buf);
                        }
                    }
                    break;
                }
            }
            break;
        }
        case WM_MOUSEMOVE:
        {
            DeleteObject(hSelectPen);						// ���������� ����
            hSelectPen = CreatePen(PS_SOLID,0,crCurUp);		// ������� ���� �� ������ �������� �����
            DeleteObject(hSelectBrush);						// ���������� �����
            hSelectBrush = CreateSolidBrush(crCurDown);		// ������� ����� �� ������ ������� �����
            SelectObject(hDC,hSelectPen);
            SelectObject(hDC,hSelectBrush);
            // ����� �������
            xMove = LOWORD(lParam);
            yMove = HIWORD(lParam);
            switch(status)
            {
                case 1:
                    if (selected)
                    {
                        xMove = LOWORD(lParam);
                        yMove = HIWORD(lParam);
                        if (numBord)
                            if (numBord!=9)
                            {
                                if (shapes[id]->i==3)
                                    shapes[id]->select_transform(numBord,xMove,yMove);
                                else
                                    shapes[id]->select_transform(numBord,xDown-xMove,yDown-yMove);

                                shapes[id]->get_info(&inf);
                                switch(shapes[id]->i)
                                {
                                    case 1:
                                        sprintf(buf,"%d\n",inf.W);
                                        SetWindowTextA(hwndEdit_width,buf);

                                        sprintf(buf,"%d\n",inf.H);
                                        SetWindowTextA(hwndEdit_height,buf);
                                        break;
                                    case 2:
                                        sprintf(buf,"%d\n",inf.R1);
                                        SetWindowTextA(hwndEdit_R1,buf);

                                        sprintf(buf,"%d\n",inf.R2);
                                        SetWindowTextA(hwndEdit_R2,buf);
                                        break;
                                    case 3:
                                        sprintf(buf,"%d\n",inf.R1);
                                        SetWindowTextA(hwndEdit_R1,buf);
                                        break;
                                }
                            }
                            else
                            {
                                double c,ugol;
                                c=sqrt((xDown-xMove)*(xDown-xMove)+(yDown-yMove)*(yDown-yMove));
                                ugol=asinh((xDown-xMove)/c);
                                ugol/=3;
                                shapes[id]->turn_obj(ugol);
                            }
                        else
                        {
                            shapes[id]->move_obj(xDown-xMove,yDown-yMove);
                            shapes[id]->get_info(&inf);

                            sprintf(buf,"%d\n",inf.X);
                            SetWindowTextA(hwndEdit_X,buf);

                            sprintf(buf,"%d\n",inf.Y);
                            SetWindowTextA(hwndEdit_Y,buf);
                        }
                        xDown = xMove;
                        yDown = yMove;
                        InvalidateRect(hwndHolst, NULL, TRUE);
                        UpdateWindow(hwndHolst);
                    }
                    break;
                case 4:
                    if (bTracking)
                    {
                        free(el_rect);
                        el_rect=NULL;
                        el_rect=new Rect(&crCurUp,&crCurDown);

                        max_x=min_x=xDown;
                        max_y=min_y=yDown;

                        if (xMove>max_x)
                            max_x=xMove;
                        else
                            if (xMove<min_x)
                                min_x=xMove;
                        if (yMove>max_y)
                            max_y=yMove;
                        else
                            if (yMove<min_y)
                                min_y=yMove;

                        el_rect->p[0].x=el_rect->p[4].x=el_rect->p[3].x=min_x;
                        el_rect->p[0].y=el_rect->p[4].y=el_rect->p[1].y=min_y;
                        el_rect->p[1].x=el_rect->p[2].x=max_x;
                        el_rect->p[2].y=el_rect->p[3].y=max_y;

                        el_rect->X=min_x+(max_x-min_x)/2.;
                        el_rect->Y=min_y+(max_y-min_y)/2.;

                        temp=el_rect;

                        InvalidateRect(hwndHolst, NULL, TRUE);
                        UpdateWindow(hwndHolst);

                        el_rect->get_info(&inf);

                        sprintf(buf,"%d\n",inf.X);
                        SetWindowTextA(hwndEdit_X,buf);

                        sprintf(buf,"%d\n",inf.Y);
                        SetWindowTextA(hwndEdit_Y,buf);

                        sprintf(buf,"%d\n",inf.W);
                        SetWindowTextA(hwndEdit_width,buf);

                        sprintf(buf,"%d\n",inf.H);
                        SetWindowTextA(hwndEdit_height,buf);
                    }
                    break;
                case 5:
                    if (bTracking)
                    {
                        free(el_ellipse);
                        el_ellipse=NULL;
                        el_ellipse=new ellipse(&crCurUp,&crCurDown);

                        max_x=min_x=xDown;
                        max_y=min_y=yDown;

                        if (xMove>max_x)
                            max_x=xMove;
                        else
                            if (xMove<min_x)
                                min_x=xMove;
                        if (yMove>max_y)
                            max_y=yMove;
                        else
                            if (yMove<min_y)
                                min_y=yMove;

                        el_ellipse->p[0].y=el_ellipse->p[1].y=min_y;
                        el_ellipse->p[1].x=el_ellipse->p[2].x=max_x;
                        el_ellipse->p[2].y=el_ellipse->p[3].y=max_y;
                        el_ellipse->p[3].x=el_ellipse->p[0].x=min_x;
                        el_ellipse->p[4].x=el_ellipse->p[0].x=min_x;
                        el_ellipse->p[4].y=el_ellipse->p[0].y=min_y;

                        el_ellipse->X=min_x+(max_x-min_x)/2.;
                        el_ellipse->Y=min_y+(max_y-min_y)/2.;

                        temp=el_ellipse;

                        el_ellipse->get_info(&inf);
                        InvalidateRect(hwndHolst, NULL, TRUE);
                        UpdateWindow(hwndHolst);


                        sprintf(buf,"%d\n",inf.X);
                        SetWindowTextA(hwndEdit_X,buf);

                        sprintf(buf,"%d\n",inf.Y);
                        SetWindowTextA(hwndEdit_Y,buf);

                        sprintf(buf,"%d\n",inf.R1);
                        SetWindowTextA(hwndEdit_R1,buf);

                        sprintf(buf,"%d\n",inf.R2);
                        SetWindowTextA(hwndEdit_R2,buf);
                    }
                    break;
                case 6:
                    if (bTracking)
                    {
                        free(el_polygon);
                        el_polygon=NULL;
                        el_polygon=new polygon(&crCurUp,&crCurDown,num_top,xDown,yDown);

                        int r=round(sqrt((xDown-xMove)*(xDown-xMove)+(yDown-yMove)*(yDown-yMove)));
                        el_polygon->R=r;
                        int a=-5;
                        int j;
                        for (j=0,i=1;i<(el_polygon->N)*2;i++)
                        {
                            if (i%2)
                            {
                                el_polygon->p[j].x=xDown+r*cos(a*3.14/180);
                                el_polygon->p[j].y=yDown-r*sin(a*3.14/180);
                                j++;
                            }
                            a=a+180/(el_polygon->N);
                        }

                        el_polygon->p[j].x=el_polygon->p[0].x;
                        el_polygon->p[j].y=el_polygon->p[0].y;

                        max_x=min_x=xDown;
                        max_y=min_y=yDown;

                        for(i=0;i<(el_polygon->N)+1;i++)
                        {
                            if (el_polygon->p[i].x>max_x)
                                max_x=el_polygon->p[i].x;
                            else
                                if (el_polygon->p[i].x<min_x)
                                    min_x=el_polygon->p[i].x;
                            if (el_polygon->p[i].y>max_y)
                                max_y=el_polygon->p[i].y;
                            else
                                if (el_polygon->p[i].y<min_y)
                                    min_y=el_polygon->p[i].y;
                        }
                        temp=el_polygon;

                        el_polygon->X=min_x+(max_x-min_x)/2.;
                        el_polygon->Y=min_y+(max_y-min_y)/2.;

                        InvalidateRect(hwndHolst, NULL, TRUE);
                        UpdateWindow(hwndHolst);

                        el_polygon->get_info(&inf);

                        sprintf(buf,"%d\n",inf.X);
                        SetWindowTextA(hwndEdit_X,buf);

                        sprintf(buf,"%d\n",inf.Y);
                        SetWindowTextA(hwndEdit_Y,buf);

                        sprintf(buf,"%d\n",inf.R1);
                        SetWindowTextA(hwndEdit_R1,buf);

                        sprintf(buf,"%d\n",inf.N);
                        SetWindowTextA(hwndEdit_num,buf);
                    }
                    break;
                case 7:
                    if (bTracking)
                    {
                        if (xMove>max_x)
                            max_x=xMove;
                        else
                            if (xMove<min_x)
                                min_x=xMove;
                        if (yMove>max_y)
                            max_y=yMove;
                        else
                            if (yMove<min_y)
                                min_y=yMove;

                        LineTo(hDC, xMove, yMove);
                        el_FreeLine->points.push_back(Point(xMove, yMove));
                    }
                    break;
            }
            break;
        }
        case WM_LBUTTONUP:
        {
            // ����� �������
            xMove = LOWORD(lParam);
            yMove = HIWORD(lParam);
            switch(status)
            {
                case 1:
                {
                    if (selected)
                    {
                        if (numBord!=0 && numBord!=9)
                        {
                            shapes[id]->move_obj(xDown-xMove,yDown-yMove);
                            shapes[id]->ver_parties();
                        }
                        selected=false;
                        numBord=0;
                        InvalidateRect(hwndHolst, NULL, TRUE);
                        UpdateWindow(hwndHolst);
                    }
                    break;
                }
                case 4:
                {
                    if (bTracking)
                    {
                        bTracking = FALSE;
                        if ((min_x==max_x)&&(min_y==max_y))
                        {
                            free(el_rect);
                            el_rect=NULL;
                            temp=NULL;
                            break;
                        }
                        id=shapes.size();

                        el_rect->vertex[0].x=el_rect->vertex[4].x=el_rect->vertex[3].x=min_x;
                        el_rect->vertex[0].y=el_rect->vertex[4].y=el_rect->vertex[1].y=min_y;
                        el_rect->vertex[1].x=el_rect->vertex[2].x=max_x;
                        el_rect->vertex[2].y=el_rect->vertex[3].y=max_y;

                        el_rect->X=min_x+(max_x-min_x)/2.;
                        el_rect->Y=min_y+(max_y-min_y)/2.;
                        el_rect->scan_node();

                        shapes.push_back(el_rect);
                        el_rect=NULL;
                        temp=NULL;
                        InvalidateRect(hwndHolst, NULL, TRUE);
                        UpdateWindow(hwndHolst);
                        SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)bufRect);
                        SendMessage(hwndList,LB_SETCURSEL,id,0);
                    }
                    break;
                }
                case 5:
                {
                    if (bTracking)
                    {
                        bTracking = FALSE;
                        if ((min_x==max_x)&&(min_y==max_y))
                        {
                            free(el_ellipse);
                            el_ellipse=NULL;
                            temp=NULL;
                            break;
                        }
                        id=shapes.size();

                        el_ellipse->X=min_x+(max_x-min_x)/2.;
                        el_ellipse->Y=min_y+(max_y-min_y)/2.;

                        shapes.push_back(el_ellipse);
                        el_ellipse=NULL;
                        temp=NULL;
                        InvalidateRect(hwndHolst, NULL, TRUE);
                        UpdateWindow(hwndHolst);
                        SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)bufEllipse);
                        SendMessage(hwndList,LB_SETCURSEL,id,0);
                    }
                    break;
                }
                case 6:
                {
                    if (bTracking)
                    {
                        bTracking = FALSE;
                        if ((min_x==max_x)&&(min_y==max_y))
                        {
                            free(el_polygon);
                            el_polygon=NULL;
                            temp=NULL;
                            break;
                        }

                        el_polygon->vertex[4].x=el_polygon->vertex[3].x=el_polygon->vertex[0].x=min_x;
                        el_polygon->vertex[4].y=el_polygon->vertex[0].y=el_polygon->vertex[1].y=min_y;
                        el_polygon->vertex[1].x=el_polygon->vertex[2].x=max_x;
                        el_polygon->vertex[2].y=el_polygon->vertex[3].y=max_y;

                        el_polygon->scan_node();

                        id=shapes.size();

                        shapes.push_back(el_polygon);
                        el_polygon=NULL;
                        temp=NULL;
                        InvalidateRect(hwndHolst, NULL, TRUE);
                        UpdateWindow(hwndHolst);
                        SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)bufPolygon);
                        SendMessage(hwndList,LB_SETCURSEL,id,0);
                    }
                    break;
                }
                case 7:
                {
                    if (bTracking)
                    {
                        bTracking = FALSE;

                        el_FreeLine->p[4].x=el_FreeLine->p[3].x=el_FreeLine->p[0].x=min_x;
                        el_FreeLine->p[4].y=el_FreeLine->p[0].y=el_FreeLine->p[1].y=min_y;
                        el_FreeLine->p[1].x=el_FreeLine->p[2].x=max_x;
                        el_FreeLine->p[2].y=el_FreeLine->p[3].y=max_y;

                        el_FreeLine->X=min_x+(max_x-min_x)/2.;
                        el_FreeLine->Y=min_y+(max_y-min_y)/2.;
                        el_FreeLine->get_info(&inf);
                        shapes.push_back(el_FreeLine);
                        el_FreeLine = NULL;
                        id=shapes.size()-1;
                        InvalidateRect(hwndHolst, NULL, TRUE);
                        UpdateWindow(hwndHolst);
                        SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)bufFreehand);
                        SendMessage(hwndList,LB_SETCURSEL,id,0);

                        sprintf(buf,"%d\n",inf.X);
                        SetWindowTextA(hwndEdit_X,buf);

                        sprintf(buf,"%d\n",inf.Y);
                        SetWindowTextA(hwndEdit_Y,buf);
                    }
                    break;
                }
                case 9:
                {
                    el_Text=NULL;
                    el_Text=new Text(&hwndHolst,&crCurUp,&crCurDown,xMove,yMove);
                    shapes.push_back(el_Text);
                    id=shapes.size()-1;
                    el_Text->get_info(&inf);
                    for(int i=0;i<inf.N;i++)
                        buf[i]=inf.st[i];
                    buf[inf.N]='\0';
                    SetWindowTextA(hwndEdit_text,buf);
                    sprintf(buf,"%d\n",inf.X);
                    SetWindowTextA(hwndEdit_X,buf);
                    sprintf(buf,"%d\n",inf.Y);
                    SetWindowTextA(hwndEdit_Y,buf);

                    InvalidateRect(hwndHolst, NULL, TRUE);
                    UpdateWindow(hwndHolst);
                    InvalidateRect(hwnd_obj_propert, NULL, TRUE);
                    UpdateWindow(hwnd_obj_propert);
                    SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)bufText);
                    SendMessage(hwndList,LB_SETCURSEL,id,0);

                    el_Text=NULL;
                    break;
                }
            }
            DeleteObject(hSelectPen);
            DeleteObject(hSelectBrush);
            break;
        }
		case WM_DESTROY:
            break;
        default:
            return DefMDIChildProc (hWnd, message, wParam, lParam);
	}
	return 0;
}

void RepaintBack()
{
    int iCount = shapes.size();

    BitBlt(hdcMemHolstBack, 0, 0, 800, 600,	NULL, 0, 0, WHITENESS);
    SelectObject(hdcMemHolstBack,GetStockObject(NULL_BRUSH));
    SelectObject(hdcMemHolstBack,GetStockObject(BLACK_PEN));
    Rectangle(hdcMemHolstBack,0,0,800,600);
	for (int i = 0; i<iCount; i++)                       //���� ��������� ������� �����
        shapes[i]->draw(&hdcMemHolstBack);
	if (temp)                                        //��������� ��������� �����(��� � �������� ���������)
        temp->draw(&hdcMemHolstBack);
    if (id!=-1)                                      //��������� ����� ��������� ��-��
    {
        if (status==1)
            shapes[id]->draw_transform(&hdcMemHolstBack,100);
        else
            shapes[id]->draw_transform(&hdcMemHolstBack,0);
    }
}

LRESULT CALLBACK WndList(HWND hwnd, UINT iMessage, WPARAM wParam, LPARAM lParam) // �������� ������� ���������
{
    if (iMessage == WM_LBUTTONUP)
	{
		id = SendMessage(hwndList, LB_GETCURSEL, 0, 0);
        InvalidateRect(hwnd_obj_propert,NULL,TRUE);
        UpdateWindow(hwnd_obj_propert);
        InvalidateRect(hwndHolst,NULL,TRUE);
        UpdateWindow(hwndHolst);
	}
	return CallWindowProc(OldWndList,hwnd,iMessage,wParam,lParam);			// ��������� � ������� ���������
}

void fill_list()
{
    SendMessage(hwndList,LB_RESETCONTENT,0,0);
    for(unsigned int i=0;i<shapes.size();i++)
    {
        switch(shapes[i]->i)
        {
            case 1:
                SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)bufRect);
                break;
            case 2:
                SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)bufEllipse);
                break;
            case 3:
                SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)bufPolygon);
                break;
            case 4:
                SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)bufFreehand);
                break;
            case 6:
                SendMessage(hwndList,LB_ADDSTRING,0,(LPARAM)bufText);
                break;
        }
    }
    if (id!=-1)
        SendMessage(hwndList,LB_SETCURSEL,id,0);
}

LRESULT CALLBACK WndLayer (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    switch(message)
    {
        case WM_CREATE:
            hwBdel=CreateWindowEx(
                                0,
                                "B_del",
                                "0",
                                WS_CHILD|WS_BORDER|WS_VISIBLE ,
                                80,2,26,26,
                                hwnd,
                                NULL,
                                hInst,
                                NULL);
            hwBup=CreateWindowEx(
                                0,
                                "B_up",
                                "1",
                                WS_CHILD|WS_BORDER|WS_VISIBLE ,
                                190,2,26,26,
                                hwnd,
                                NULL,
                                hInst,
                                NULL);
            hwBdown=CreateWindowEx(
                                0,
                                "B_down",
                                "2",
                                WS_CHILD|WS_BORDER|WS_VISIBLE ,
                                220,2,26,26,
                                hwnd,
                                NULL,
                                hInst,
                                NULL);
            ShowWindow(hwBdel,SW_NORMAL);
            ShowWindow(hwBup,SW_NORMAL);
            ShowWindow(hwBdown,SW_NORMAL);
        case WM_PAINT:
            BeginPaint(hwnd, &ps);
            EndPaint(hwnd, &ps);
            break;
        case WM_COMMAND:
            if ((HIWORD(wParam)==0))
            switch(LOWORD(wParam))
            {
                case MI_LAYER_DEL:
                    if (id!=-1)
                    {
                        shapes.erase(shapes.begin() + id);
                        id=-1;
                        fill_list();
                        InvalidateRect(hwndHolst, NULL, TRUE);
                        UpdateWindow(hwndHolst);
                        InvalidateRect(hwnd_obj_propert,NULL,TRUE);
                        UpdateWindow(hwnd_obj_propert);
                    }
                    break;
                case MI_LAYER_UP1:
                    if (id!=-1)
                        SendMessage(hwndMain,WM_COMMAND,MI_LAYER_UP1,0);
                    break;
                case MI_LAYER_DOWN1:
                    if (id!=-1)
                        SendMessage(hwndMain,WM_COMMAND,MI_LAYER_DOWN1,0);
                    break;

            }
            break;
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK WndB0 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBITMAP hBit;
    BITMAP bm;
    HDC hDC;
    HDC hMemDC;
    HWND hwnd1;

    switch(message)
    {
        case WM_PAINT:
            if (status!=1)
                hBit=(HBITMAP)LoadImage (hInst, "resources//q0.1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);\
            else
                hBit=(HBITMAP)LoadImage (hInst, "resources//q0.2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            GetObject (hBit, sizeof(BITMAP), &bm);
            hDC=GetDC(hwnd1);
            hMemDC = CreateCompatibleDC(GetDC(hwnd1));
            SelectBitmap(hMemDC,hBit);
            hBit=NULL;
            ReleaseDC(hwnd1,hDC);
            hDC= BeginPaint(hwnd,&ps);
            BitBlt(hDC, 0, 0, bm.bmWidth, bm.bmHeight, hMemDC, 0, 0, SRCCOPY);
            EndPaint(hwnd,&ps);
            break;
        case WM_LBUTTONUP:
            status=1;
            if (id!=-1)
            {
                InvalidateRect(hwndHolst,NULL,TRUE);
                UpdateWindow(hwndHolst);
            }
            InvalidateRect(hwnd_obj_propert,NULL,TRUE);
            UpdateWindow(hwnd_obj_propert);
            InvalidateRect(hwnd_list,NULL,TRUE);
            UpdateWindow(hwnd_list);
            break;
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK WndB3 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBITMAP hBit;
    BITMAP bm;
    HDC hDC;
    HDC hMemDC;
    HWND hwnd1;

    switch(message)
    {
        case WM_PAINT:
            if (status!=4)
                hBit=(HBITMAP)LoadImage (hInst, "resources//q3.1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            else
                hBit=(HBITMAP)LoadImage (hInst, "resources//q3.2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            GetObject (hBit, sizeof(BITMAP), &bm);
            hDC=GetDC(hwnd1);
            hMemDC = CreateCompatibleDC(GetDC(hwnd1));
            SelectBitmap(hMemDC,hBit);
            hBit=NULL;
            ReleaseDC(hwnd1,hDC);
            hDC= BeginPaint(hwnd,&ps);
            BitBlt(hDC, 0, 0, bm.bmWidth, bm.bmHeight, hMemDC, 0, 0, SRCCOPY);
            EndPaint(hwnd,&ps);
            break;
        case WM_LBUTTONUP:

            if (status!=4)
            {
                status=4;
                id=-1;
                InvalidateRect(hwndHolst,NULL,TRUE);
                UpdateWindow(hwndHolst);
                InvalidateRect(hwnd_obj_propert,NULL,TRUE);
                UpdateWindow(hwnd_obj_propert);
            }
            InvalidateRect(hwnd_list,NULL,TRUE);
            UpdateWindow(hwnd_list);
            break;
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK WndB4 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBITMAP hBit;
    BITMAP bm;
    HDC hDC;
    HDC hMemDC;
    HWND hwnd1;

    switch(message)
    {
        case WM_PAINT:
            if (status!=5)
                hBit=(HBITMAP)LoadImage (hInst, "resources//q4.1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            else
                hBit=(HBITMAP)LoadImage (hInst, "resources//q4.2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            GetObject (hBit, sizeof(BITMAP), &bm);
            hDC=GetDC(hwnd1);
            hMemDC = CreateCompatibleDC(GetDC(hwnd1));
            SelectBitmap(hMemDC,hBit);
            hBit=NULL;
            ReleaseDC(hwnd1,hDC);
            hDC= BeginPaint(hwnd,&ps);
            BitBlt(hDC, 0, 0, bm.bmWidth, bm.bmHeight, hMemDC, 0, 0, SRCCOPY);
            EndPaint(hwnd,&ps);
            break;
        case WM_LBUTTONUP:
            if (status!=5)
            {
                status=5;
                id=-1;
                InvalidateRect(hwndHolst,NULL,TRUE);
                UpdateWindow(hwndHolst);
                InvalidateRect(hwnd_obj_propert,NULL,TRUE);
                UpdateWindow(hwnd_obj_propert);
            }
            InvalidateRect(hwnd_list,NULL,TRUE);
            UpdateWindow(hwnd_list);
            break;
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK WndB5 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBITMAP hBit;
    BITMAP bm;
    HDC hDC;
    HDC hMemDC;
    HWND hwnd1;

    switch(message)
    {
        case WM_PAINT:
            if (status!=6)
                hBit=(HBITMAP)LoadImage (hInst, "resources//q5.1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            else
                hBit=(HBITMAP)LoadImage (hInst, "resources//q5.2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            GetObject (hBit, sizeof(BITMAP), &bm);
            hDC=GetDC(hwnd1);
            hMemDC = CreateCompatibleDC(GetDC(hwnd1));
            SelectBitmap(hMemDC,hBit);
            hBit=NULL;
            ReleaseDC(hwnd1,hDC);
            hDC= BeginPaint(hwnd,&ps);
            BitBlt(hDC, 0, 0, bm.bmWidth, bm.bmHeight, hMemDC, 0, 0, SRCCOPY);
            EndPaint(hwnd,&ps);
            break;
        case WM_LBUTTONUP:
            if (status!=6)
            {
                status=6;
                id=-1;
                InvalidateRect(hwndHolst,NULL,TRUE);
                UpdateWindow(hwndHolst);
                InvalidateRect(hwnd_obj_propert,NULL,TRUE);
                UpdateWindow(hwnd_obj_propert);
            }
            InvalidateRect(hwnd_list,NULL,TRUE);
            UpdateWindow(hwnd_list);
            break;
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK WndB6 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBITMAP hBit;
    BITMAP bm;
    HDC hDC;
    HDC hMemDC;
    HWND hwnd1;

    switch(message)
    {
        case WM_PAINT:
            if (status!=7)
                hBit=(HBITMAP)LoadImage (hInst, "resources//q6.1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            else
                hBit=(HBITMAP)LoadImage (hInst, "resources//q6.2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            GetObject (hBit, sizeof(BITMAP), &bm);
            hDC=GetDC(hwnd1);
            hMemDC = CreateCompatibleDC(GetDC(hwnd1));
            SelectBitmap(hMemDC,hBit);
            hBit=NULL;
            ReleaseDC(hwnd1,hDC);
            hDC= BeginPaint(hwnd,&ps);
            BitBlt(hDC, 0, 0, bm.bmWidth, bm.bmHeight, hMemDC, 0, 0, SRCCOPY);
            EndPaint(hwnd,&ps);
            break;
        case WM_LBUTTONUP:

            if (status!=7)
            {
                status=7;
                id=-1;
                InvalidateRect(hwndHolst,NULL,TRUE);
                UpdateWindow(hwndHolst);
                InvalidateRect(hwnd_obj_propert,NULL,TRUE);
                UpdateWindow(hwnd_obj_propert);
            }
            InvalidateRect(hwnd_list,NULL,TRUE);
            UpdateWindow(hwnd_list);
            break;
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK WndB8 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBITMAP hBit;
    BITMAP bm;
    HDC hDC;
    HDC hMemDC;
    HWND hwnd1;

    switch(message)
    {
        case WM_PAINT:
            if (status!=9)
                hBit=(HBITMAP)LoadImage (hInst, "resources//q8.1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            else
                hBit=(HBITMAP)LoadImage (hInst, "resources//q8.2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            GetObject (hBit, sizeof(BITMAP), &bm);
            hDC=GetDC(hwnd1);
            hMemDC = CreateCompatibleDC(GetDC(hwnd1));
            SelectBitmap(hMemDC,hBit);
            hBit=NULL;
            ReleaseDC(hwnd1,hDC);
            hDC= BeginPaint(hwnd,&ps);
            BitBlt(hDC, 0, 0, bm.bmWidth, bm.bmHeight, hMemDC, 0, 0, SRCCOPY);
            EndPaint(hwnd,&ps);
            break;
        case WM_LBUTTONUP:

            if (status!=9)
            {
                status=9;
                id=-1;
                InvalidateRect(hwndHolst,NULL,TRUE);
                UpdateWindow(hwndHolst);
                InvalidateRect(hwnd_obj_propert,NULL,TRUE);
                UpdateWindow(hwnd_obj_propert);
            }
            InvalidateRect(hwnd_list,NULL,TRUE);
            UpdateWindow(hwnd_list);
            break;
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK WndB9 (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBITMAP hBit;
    BITMAP bm;
    HDC hDC;
    HDC hMemDC;
    HWND hwnd1;

    switch(message)
    {
        case WM_PAINT:
            if (status!=10)
                hBit=(HBITMAP)LoadImage (hInst, "resources//q9.1.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            else
                hBit=(HBITMAP)LoadImage (hInst, "resources//q9.2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            GetObject (hBit, sizeof(BITMAP), &bm);
            hDC=GetDC(hwnd1);
            hMemDC = CreateCompatibleDC(GetDC(hwnd1));
            SelectBitmap(hMemDC,hBit);
            hBit=NULL;
            ReleaseDC(hwnd1,hDC);
            hDC= BeginPaint(hwnd,&ps);
            BitBlt(hDC, 0, 0, bm.bmWidth, bm.bmHeight, hMemDC, 0, 0, SRCCOPY);
            EndPaint(hwnd,&ps);
            break;
        case WM_LBUTTONUP:

            if (status!=10)
            {
                status=10;
                id=-1;
                InvalidateRect(hwndHolst,NULL,TRUE);
                UpdateWindow(hwndHolst);
                InvalidateRect(hwnd_obj_propert,NULL,TRUE);
                UpdateWindow(hwnd_obj_propert);
            }
            InvalidateRect(hwnd_list,NULL,TRUE);
            UpdateWindow(hwnd_list);
            break;
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK WndB_del (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBITMAP hBit;
    BITMAP bm;
    HDC hDC;
    HDC hMemDC;
    HWND hwnd1;
    static bool f=1;
    switch(message)
    {
        case WM_PAINT:
            if (f)
                hBit=(HBITMAP)LoadImage (hInst, "resources//del.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            else
                hBit=(HBITMAP)LoadImage (hInst, "resources//del2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            GetObject (hBit, sizeof(BITMAP), &bm);
            hDC=GetDC(hwnd1);
            hMemDC = CreateCompatibleDC(GetDC(hwnd1));
            SelectBitmap(hMemDC,hBit);
            hBit=NULL;
            ReleaseDC(hwnd1,hDC);
            hDC= BeginPaint(hwnd,&ps);
            BitBlt(hDC, 0, 0, bm.bmWidth, bm.bmHeight, hMemDC, 0, 0, SRCCOPY);
            EndPaint(hwnd,&ps);
            break;
        case WM_LBUTTONDOWN:
            f= (f ? false : true);
            InvalidateRect(hwBdel,NULL,TRUE);
            UpdateWindow(hwBdel);
            break;
        case WM_LBUTTONUP:
            f= (f ? false : true);
            InvalidateRect(hwBdel,NULL,TRUE);
            UpdateWindow(hwBdel);
            SendMessage(hwndLayer,WM_COMMAND,MI_LAYER_DEL,0);
            break;
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK WndB_up (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBITMAP hBit;
    BITMAP bm;
    HDC hDC;
    HDC hMemDC;
    HWND hwnd1;
    static bool f=1;
    switch(message)
    {
        case WM_PAINT:
            if (f)
                hBit=(HBITMAP)LoadImage (hInst, "resources//down.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            else
                hBit=(HBITMAP)LoadImage (hInst, "resources//down2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            GetObject (hBit, sizeof(BITMAP), &bm);
            hDC=GetDC(hwnd1);
            hMemDC = CreateCompatibleDC(GetDC(hwnd1));
            SelectBitmap(hMemDC,hBit);
            hBit=NULL;
            ReleaseDC(hwnd1,hDC);
            hDC= BeginPaint(hwnd,&ps);
            BitBlt(hDC, 0, 0, bm.bmWidth, bm.bmHeight, hMemDC, 0, 0, SRCCOPY);
            EndPaint(hwnd,&ps);
            break;
        case WM_LBUTTONDOWN:
            f= (f ? false : true);
            InvalidateRect(hwBup,NULL,TRUE);
            UpdateWindow(hwBup);
            break;
        case WM_LBUTTONUP:
            f= (f ? false : true);
            InvalidateRect(hwBup,NULL,TRUE);
            UpdateWindow(hwBup);
            if (id!=-1)
                SendMessage(hwndMain,WM_COMMAND,MI_LAYER_UP1,0);
            break;
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK WndB_down (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBITMAP hBit;
    BITMAP bm;
    HDC hDC;
    HDC hMemDC;
    HWND hwnd1;
    static bool f=1;
    switch(message)
    {
        case WM_PAINT:
            if (f)
                hBit=(HBITMAP)LoadImage (hInst, "resources//up.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            else
                hBit=(HBITMAP)LoadImage (hInst, "resources//up2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            hDC=GetDC(hwnd1);
            hMemDC = CreateCompatibleDC(GetDC(hwnd1));
            SelectBitmap(hMemDC,hBit);
            hBit=NULL;
            ReleaseDC(hwnd1,hDC);
            hDC= BeginPaint(hwnd,&ps);
            BitBlt(hDC, 0, 0, bm.bmWidth, bm.bmHeight, hMemDC, 0, 0, SRCCOPY);
            EndPaint(hwnd,&ps);
            break;
        case WM_LBUTTONDOWN:
            f= (f ? false : true);
            InvalidateRect(hwBdown,NULL,TRUE);
            UpdateWindow(hwBdown);
            break;
        case WM_LBUTTONUP:
            f= (f ? false : true);
            InvalidateRect(hwBdown,NULL,TRUE);
            UpdateWindow(hwBdown);
            if (id!=-1)
                SendMessage(hwndMain,WM_COMMAND,MI_LAYER_DOWN1,0);
            break;
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK WndB_enter (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBITMAP hBit;
    BITMAP bm;
    HDC hDC;
    HDC hMemDC;
    HWND hwnd1;
    static bool f=1;
    switch(message)
    {
        case WM_PAINT:
            if (f)
                hBit=(HBITMAP)LoadImage (hInst, "resources//enter.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            else
                hBit=(HBITMAP)LoadImage (hInst, "resources//enter2.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);GetObject (hBit, sizeof(BITMAP), &bm);
            hDC=GetDC(hwnd1);
            hMemDC = CreateCompatibleDC(GetDC(hwnd1));
            SelectBitmap(hMemDC,hBit);
            hBit=NULL;
            ReleaseDC(hwnd1,hDC);
            hDC= BeginPaint(hwnd,&ps);
            BitBlt(hDC, 0, 0, bm.bmWidth, bm.bmHeight, hMemDC, 0, 0, SRCCOPY);
            EndPaint(hwnd,&ps);
            break;
        case WM_LBUTTONDOWN:
            f= (f ? false : true);
            InvalidateRect(hwndBut_enter,NULL,TRUE);
            UpdateWindow(hwndBut_enter);
            break;
        case WM_LBUTTONUP:
            f= (f ? false : true);
            InvalidateRect(hwndBut_enter,NULL,TRUE);
            UpdateWindow(hwndBut_enter);
            if (id!=-1)
                SendMessage(hwnd_obj_propert,WM_COMMAND,BT_ENTER,0);
            break;
        case WM_DESTROY:
            DestroyWindow(hwnd);
        default:
            return DefMDIChildProc (hwnd, message, wParam, lParam);
    }
    return 0;
}

void save_svg(char *name)
{
    ofstream f;
    int r,g,b;
    INFO inf;

    f.open(name);
	f<<"<?xml version=\"1.0\" encoding=\"windows-1251\" standalone=\"no\"?>"<<endl;
	f<<"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\""<<endl;
	f<<"\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">"<<endl;
	f<<"<svg width=\"800\" height=\"600\""<<endl;
	f<<"xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">"<<endl;

    for(unsigned int i=0;i<shapes.size();i++)
    {
        shapes[i]->get_info(&inf);
        switch(shapes[i]->i)
        {
            case 1:
            {
                f<<"<polygon points=\"";
                f<<shapes[i]->p[0].x<<","<<shapes[i]->p[0].y<<" ";
                f<<shapes[i]->p[1].x<<","<<shapes[i]->p[1].y<<" ";
                f<<shapes[i]->p[2].x<<","<<shapes[i]->p[2].y<<" ";
                f<<shapes[i]->p[3].x<<","<<shapes[i]->p[3].y<<"\" ";

                r=GetRValue(inf.c1);
                g=GetGValue(inf.c1);
                b=GetBValue(inf.c1);

                f<<" style=\"fill:rgb("<<r<<","<<g<<","<<b<<");";

                r=GetRValue(inf.c2);
                g=GetGValue(inf.c2);
                b=GetBValue(inf.c2);

                f<<" stroke:rgb("<<r<<","<<g<<","<<b<<");";

                f<<" stroke-width:2;\"/>"<<endl;
                break;
            }
            case 2:
            {
                f<<"<ellipse cx=\""<<inf.X<<"\" cy=\""<<inf.Y<<"\" rx=\""<<inf.R1<<"\" ry=\""<<inf.R2<<"\"";

                r=GetRValue(inf.c1);
                g=GetGValue(inf.c1);
                b=GetBValue(inf.c1);

                f<<" style=\"fill:rgb("<<r<<","<<g<<","<<b<<");";

                r=GetRValue(inf.c2);
                g=GetGValue(inf.c2);
                b=GetBValue(inf.c2);

                f<<" stroke:rgb("<<r<<","<<g<<","<<b<<");";

                f<<" stroke-width:2;\"/>"<<endl;
                break;
            }
            case 3:
            {
                f<<"<polygon points=\"";

                for(int q=0;q<inf.N;q++)
                {
                    f<<shapes[i]->p[q].x<<",";
                    f<<shapes[i]->p[q].y<<" ";
                }

                r=GetRValue(inf.c1);
                g=GetGValue(inf.c1);
                b=GetBValue(inf.c1);

                f<<"\" style=\"fill:rgb("<<r<<","<<g<<","<<b<<");";

                r=GetRValue(inf.c2);
                g=GetGValue(inf.c2);
                b=GetBValue(inf.c2);

                f<<" stroke:rgb("<<r<<","<<g<<","<<b<<");";

                f<<" stroke-width:2;\"/>"<<endl;
                break;
            }
            case 4:
            {
                f<<"<polyline points=\"";
                for(shapes[i]->it=shapes[i]->points.begin();shapes[i]->it!=shapes[i]->points.end();shapes[i]->it++)
                {
                    f<<shapes[i]->it->x<<",";
                    f<<shapes[i]->it->y<<" ";
                }

                r=GetRValue(inf.c1);
                g=GetGValue(inf.c1);
                b=GetBValue(inf.c1);

                f<<"\" style=\"fill:none; stroke:rgb("<<r<<","<<g<<","<<b<<");";

                f<<" stroke-width:2;\"/>"<<endl;
                break;
            }
            case 6:
            {
                if (!inf.N)
                    break;
                f<<"<text x=\""<<inf.X<<"\" y=\""<<inf.Y<<"\" ";
                r=GetRValue(inf.c1);
                g=GetGValue(inf.c1);
                b=GetBValue(inf.c1);

                f<<"style=\"fill:rgb("<<r<<","<<g<<","<<b<<"); ";

                f<<"font-family:"<<shapes[i]->cf.lpLogFont->lfFaceName<<"; ";
                f<<"font-size:"<<shapes[i]->cf.iPointSize/10<<"px;";
                if (shapes[i]->cf.lpLogFont->lfItalic)
                    f<<"font-style:italic; ";
                if (shapes[i]->cf.lpLogFont->lfUnderline)
                    f<<"text-decoration: underline; ";
                if (shapes[i]->cf.lpLogFont->lfStrikeOut)
                    f<<"text-decoration: line-through; ";
                f<<"font-weight:"<<shapes[i]->cf.lpLogFont->lfWeight<<"; \">"<<endl;
                f<<"    <tspan x=\""<<inf.X<<"\" dy=\"1em\">"<<endl<<"        ";
                int k;
                bool flag=1;
                for(int j=0;j<inf.N;j++)
                {
                    if (inf.st[j]!='\n')
                        f<<inf.st[j];
                    else
                    {
                        f<<"    </tspan>"<<endl;
                        int q=1;
                        for(k=j+2;k<inf.N;k++)
                        {
                            if (inf.st[k]=='\n')
                            {
                                k++;
                                q++;
                                j+=2;
                            }
                            else
                                break;
                        }
                        j++;
                        if (j<inf.N)
                        {
                            f<<"    <tspan x=\""<<inf.X<<"\" dy=\""<<q<<"em\">"<<endl;
                            f<<"        "<<inf.st[j];
                            flag=1;
                        }
                        else
                            flag=0;
                    }
                }
                if (flag)
                    f<<endl<<"    </tspan>"<<endl;
                f<<"</text>"<<endl;
                break;
            }
        }
    }
    f<<"</svg>"<<endl;
    f.close();
    ShellExecute(NULL, NULL, name, NULL, NULL, SW_SHOWNORMAL);
}

void save_as(char *name)
{
    INFO inf;
    ofstream out(name,ios::binary|ios::out);
    int i,j,x,y,rgb,N=shapes.size();
    out.write((char*)&N,sizeof N);
    for(i=0;i<N;i++)
    {
        shapes[i]->get_info(&inf);
        switch(shapes[i]->i)
        {
            case 1:
            {
                out.write((char*)&inf.i,sizeof inf.i);

                rgb=GetRValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetGValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetBValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);

                rgb=GetRValue(inf.c2);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetGValue(inf.c2);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetBValue(inf.c2);
                out.write((char*)&rgb,sizeof rgb);

                for(j=0;j<5;j++)
                {
                    x=shapes[i]->p[j].x;
                    y=shapes[i]->p[j].y;
                    out.write((char*)&x,sizeof x);
                    out.write((char*)&y,sizeof y);

                    x=shapes[i]->vertex[j].x;
                    y=shapes[i]->vertex[j].y;
                    out.write((char*)&x,sizeof x);
                    out.write((char*)&y,sizeof y);
                }
                x=inf.X;
                y=inf.Y;
                out.write((char*)&x,sizeof x);
                out.write((char*)&y,sizeof y);
                break;
            }
            case 2:
            {
                out.write((char*)&inf.i,sizeof inf.i);

                rgb=GetRValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetGValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetBValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);

                rgb=GetRValue(inf.c2);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetGValue(inf.c2);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetBValue(inf.c2);
                out.write((char*)&rgb,sizeof rgb);

                for(j=0;j<5;j++)
                {
                    x=shapes[i]->p[j].x;
                    y=shapes[i]->p[j].y;
                    out.write((char*)&x,sizeof x);
                    out.write((char*)&y,sizeof y);
                }
                x=inf.X;
                y=inf.Y;
                out.write((char*)&x,sizeof x);
                out.write((char*)&y,sizeof y);
                break;
            }
            case 3:
            {
                out.write((char*)&inf.i,sizeof inf.i);

                rgb=GetRValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetGValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetBValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);

                rgb=GetRValue(inf.c2);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetGValue(inf.c2);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetBValue(inf.c2);
                out.write((char*)&rgb,sizeof rgb);

                //���������� ������
                out.write((char*)&inf.X,sizeof inf.X);
                out.write((char*)&inf.Y,sizeof inf.Y);

                //������
                out.write((char*)&inf.R1,sizeof inf.R1);

                //���-�� ������
                out.write((char*)&inf.N,sizeof inf.N);

                for(j=0;j<inf.N+1;j++)
                {
                    x=shapes[i]->p[j].x;
                    y=shapes[i]->p[j].y;
                    out.write((char*)&x,sizeof x);
                    out.write((char*)&y,sizeof y);
                }

                for(j=0;j<5;j++)
                {
                    x=shapes[i]->vertex[j].x;
                    y=shapes[i]->vertex[j].y;
                    out.write((char*)&x,sizeof x);
                    out.write((char*)&y,sizeof y);
                }
                break;
            }
            case 4:
            {
                out.write((char*)&inf.i,sizeof inf.i);
                //����
                rgb=GetRValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetGValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetBValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);
                //���������� ������
                out.write((char*)&inf.X,sizeof inf.X);
                out.write((char*)&inf.Y,sizeof inf.Y);
                //���������� ������ ����������� �����
                for(j=0;j<5;j++)
                {
                    x=shapes[i]->p[j].x;
                    y=shapes[i]->p[j].y;
                    out.write((char*)&x,sizeof x);
                    out.write((char*)&y,sizeof y);
                }
                //���-�� �����
                out.write((char*)&inf.N,sizeof inf.N);
                //���������� �����//shapes[i]->it!=shapes[i]->points.end();
                for(j=0,shapes[i]->it=shapes[i]->points.begin();j<inf.N;shapes[i]->it++,j++)
                {
                    x=shapes[i]->it->x;
                    y=shapes[i]->it->y;
                    out.write((char*)&x,sizeof x);
                    out.write((char*)&y,sizeof y);
                }
                break;
            }
            case 6:
            {
                out.write((char*)&inf.i,sizeof inf.i);
                //COLOR
                rgb=GetRValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetGValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetBValue(inf.c1);
                out.write((char*)&rgb,sizeof rgb);
                //COLOR
                rgb=GetRValue(inf.c2);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetGValue(inf.c2);
                out.write((char*)&rgb,sizeof rgb);
                rgb=GetBValue(inf.c2);
                out.write((char*)&rgb,sizeof rgb);
                //coordinates
                out.write((char*)&inf.X,sizeof inf.X);
                out.write((char*)&inf.Y,sizeof inf.Y);
                //struct LOGFONT
                out.write((char*)&shapes[i]->lf,sizeof shapes[i]->lf);
                //numbre of character
                out.write((char*)&inf.N,sizeof inf.N);
                //text
                for(int j=0;j<inf.N+1;j++)
                    out.write((char*)&inf.st[j],sizeof inf.st[j]);
            }
            break;
        }
    }
    out.close();
}

void open_file(char *name)
{
    INFO inf;
    ifstream  in(name,ios::binary|ios::in);
    int i,j,x,y,N,kind,R,num;
    int r,g,b; //����
    COLORREF	crCurUp;
    COLORREF	crCurDown;

    SetWindowText(hwndMain,TitleFile2);
    in.read((char*)&N,sizeof N);
    for(i=0;i<N;i++)
    {
        in.read((char*)&kind,sizeof kind);
        switch(kind)
        {
            case 1:
            {
                in.read((char*)&r,sizeof r);
                in.read((char*)&g,sizeof g);
                in.read((char*)&b,sizeof b);
                crCurUp=RGB(r,g,b);
                in.read((char*)&r,sizeof r);
                in.read((char*)&g,sizeof g);
                in.read((char*)&b,sizeof b);
                crCurDown=RGB(r,g,b);

                el_rect=NULL;
                el_rect=new Rect(&crCurUp,&crCurDown);

                for(j=0;j<5;j++)
                {
                    in.read((char*)&x,sizeof x);
                    in.read((char*)&y,sizeof y);
                    el_rect->p[j].x=x;
                    el_rect->p[j].y=y;
                    in.read((char*)&x,sizeof x);
                    in.read((char*)&y,sizeof y);
                    el_rect->vertex[j].x=x;
                    el_rect->vertex[j].y=y;
                }
                in.read((char*)&x,sizeof x);
                in.read((char*)&y,sizeof y);
                el_rect->X=x;
                el_rect->Y=y;

                el_rect->scan_node();

                shapes.push_back(el_rect);
                el_rect=NULL;
            }
            break;
            case 2:
            {
                in.read((char*)&r,sizeof r);
                in.read((char*)&g,sizeof g);
                in.read((char*)&b,sizeof b);
                crCurUp=RGB(r,g,b);
                in.read((char*)&r,sizeof r);
                in.read((char*)&g,sizeof g);
                in.read((char*)&b,sizeof b);
                crCurDown=RGB(r,g,b);

                el_ellipse=NULL;
                el_ellipse=new ellipse(&crCurUp,&crCurDown);

                for(j=0;j<5;j++)
                {
                    in.read((char*)&x,sizeof x);
                    in.read((char*)&y,sizeof y);
                    el_ellipse->p[j].x=x;
                    el_ellipse->p[j].y=y;
                }
                in.read((char*)&x,sizeof x);
                in.read((char*)&y,sizeof y);
                el_ellipse->X=x;
                el_ellipse->Y=y;

                el_ellipse->scan_node();

                shapes.push_back(el_ellipse);
                el_ellipse=NULL;
            }
            break;
            case 3:
            {
                in.read((char*)&r,sizeof r);
                in.read((char*)&g,sizeof g);
                in.read((char*)&b,sizeof b);
                crCurUp=RGB(r,g,b);
                in.read((char*)&r,sizeof r);
                in.read((char*)&g,sizeof g);
                in.read((char*)&b,sizeof b);
                crCurDown=RGB(r,g,b);

                in.read((char*)&x,sizeof x);
                in.read((char*)&y,sizeof y);

                in.read((char*)&R,sizeof R);
                in.read((char*)&num,sizeof num);
                el_polygon=new polygon(&crCurUp,&crCurDown,num,x,y);
                el_polygon->R=R;

                for(j=0;j<num+1;j++)
                {
                    in.read((char*)&x,sizeof x);
                    in.read((char*)&y,sizeof y);
                    el_polygon->p[j].x=x;
                    el_polygon->p[j].y=y;
                }

                for(j=0;j<5;j++)
                {
                    in.read((char*)&x,sizeof x);
                    in.read((char*)&y,sizeof y);
                    el_polygon->vertex[j].x=x;
                    el_polygon->vertex[j].y=y;
                }

                el_polygon->scan_node();

                shapes.push_back(el_polygon);
                el_polygon=NULL;
            }
            break;
            case 4:
            {
                //color
                in.read((char*)&r,sizeof r);
                in.read((char*)&g,sizeof g);
                in.read((char*)&b,sizeof b);
                crCurUp=RGB(r,g,b);
                el_FreeLine=new FreeLine(&crCurUp);
                //coordinates of the center
                in.read((char*)&x,sizeof x);
                in.read((char*)&y,sizeof y);
                el_FreeLine->X=x;
                el_FreeLine->Y=y;
                //frame
                for(j=0;j<5;j++)
                {
                    in.read((char*)&x,sizeof x);
                    in.read((char*)&y,sizeof y);
                    el_FreeLine->p[j].x=x;
                    el_FreeLine->p[j].y=y;
                }
                //number of tops
                in.read((char*)&num,sizeof num);
                for(j=0;j<num;j++)
                {
                    in.read((char*)&x,sizeof x);
                    in.read((char*)&y,sizeof y);
                    el_FreeLine->points.push_back(Point(x, y));
                }

                //el_FreeLine->scan_node();

                shapes.push_back(el_FreeLine);
                el_FreeLine=NULL;
            }
            break;
            case 6:
            {
                //color
                in.read((char*)&r,sizeof r);
                in.read((char*)&g,sizeof g);
                in.read((char*)&b,sizeof b);
                crCurUp=RGB(r,g,b);
                in.read((char*)&r,sizeof r);
                in.read((char*)&g,sizeof g);
                in.read((char*)&b,sizeof b);
                crCurDown=RGB(r,g,b);

                in.read((char*)&x,sizeof x);
                in.read((char*)&y,sizeof y);
                el_Text=new Text(&hwndHolst,&crCurUp,&crCurDown,x,y);
                in.read((char*)&el_Text->lf,sizeof el_Text->lf);
                in.read((char*)&num,sizeof num);
                char buf[num+1];
                for(int j=0;j<num+1;j++)
                    in.read((char*)&buf[j],sizeof buf[j]);
                buf[num]='\0';
                el_Text->add_char(buf,num+1);
                shapes.push_back(el_Text);
                el_Text=NULL;
            }
            break;
        }
    }
    in.close();
}
